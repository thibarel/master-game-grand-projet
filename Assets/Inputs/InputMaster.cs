//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.3.0
//     from Assets/Inputs/InputMaster.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public partial class @InputMaster : IInputActionCollection2, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputMaster"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""92b4d7dc-9c68-4905-b5af-d6b7dde50f5c"",
            ""actions"": [
                {
                    ""name"": ""Figure_1"",
                    ""type"": ""Button"",
                    ""id"": ""2d825b40-54ab-49c2-ac5f-a59a5b7945a9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press"",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Figure_2"",
                    ""type"": ""Button"",
                    ""id"": ""ebded524-3efe-4ad8-9f44-8babbe94ef9f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press"",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""ActivateRagdoll"",
                    ""type"": ""Button"",
                    ""id"": ""664de79a-2d0e-43b0-ba6f-13d2eea7310c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Torque"",
                    ""type"": ""Value"",
                    ""id"": ""fba58a20-de6a-41a4-9817-b5369a27e211"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                },
                {
                    ""name"": ""Join"",
                    ""type"": ""Button"",
                    ""id"": ""41630cb1-c8b9-425a-a69e-e6679e6361fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press"",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""cbededcf-e5a7-4961-ab54-d0da23ddab2d"",
                    ""expectedControlType"": ""Analog"",
                    ""processors"": """",
                    ""interactions"": """",
                    ""initialStateCheck"": true
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""9d371ad8-443b-4827-a160-a32ba274ee2c"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Torque"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""9c34ead3-7d73-4c11-9ee9-ae5cd6a37669"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Torque"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""32c23b57-e823-46a4-817d-c418adaa10bc"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Torque"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a1240eb0-8307-4fa2-8f88-fdce158fe0d0"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Torque"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""ba32db1d-8796-4a17-bcba-129c6fb09e8d"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Figure_1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""28e216b7-4502-416b-bdaf-6f6e69e2ac19"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Figure_2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2c081639-1961-40b3-8409-7ecbafa438ed"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""ActivateRagdoll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0e5d76d5-ba8a-4ba1-a87d-9b24f2d4c9f9"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Join"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fd387c1a-3dbc-4a83-ae79-a66e1e028f9a"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Figure_1 = m_Player.FindAction("Figure_1", throwIfNotFound: true);
        m_Player_Figure_2 = m_Player.FindAction("Figure_2", throwIfNotFound: true);
        m_Player_ActivateRagdoll = m_Player.FindAction("ActivateRagdoll", throwIfNotFound: true);
        m_Player_Torque = m_Player.FindAction("Torque", throwIfNotFound: true);
        m_Player_Join = m_Player.FindAction("Join", throwIfNotFound: true);
        m_Player_Movement = m_Player.FindAction("Movement", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }
    public IEnumerable<InputBinding> bindings => asset.bindings;

    public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
    {
        return asset.FindAction(actionNameOrId, throwIfNotFound);
    }
    public int FindBinding(InputBinding bindingMask, out InputAction action)
    {
        return asset.FindBinding(bindingMask, out action);
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Figure_1;
    private readonly InputAction m_Player_Figure_2;
    private readonly InputAction m_Player_ActivateRagdoll;
    private readonly InputAction m_Player_Torque;
    private readonly InputAction m_Player_Join;
    private readonly InputAction m_Player_Movement;
    public struct PlayerActions
    {
        private @InputMaster m_Wrapper;
        public PlayerActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Figure_1 => m_Wrapper.m_Player_Figure_1;
        public InputAction @Figure_2 => m_Wrapper.m_Player_Figure_2;
        public InputAction @ActivateRagdoll => m_Wrapper.m_Player_ActivateRagdoll;
        public InputAction @Torque => m_Wrapper.m_Player_Torque;
        public InputAction @Join => m_Wrapper.m_Player_Join;
        public InputAction @Movement => m_Wrapper.m_Player_Movement;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Figure_1.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFigure_1;
                @Figure_1.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFigure_1;
                @Figure_1.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFigure_1;
                @Figure_2.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFigure_2;
                @Figure_2.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFigure_2;
                @Figure_2.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFigure_2;
                @ActivateRagdoll.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnActivateRagdoll;
                @ActivateRagdoll.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnActivateRagdoll;
                @ActivateRagdoll.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnActivateRagdoll;
                @Torque.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTorque;
                @Torque.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTorque;
                @Torque.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTorque;
                @Join.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJoin;
                @Join.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJoin;
                @Join.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJoin;
                @Movement.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Figure_1.started += instance.OnFigure_1;
                @Figure_1.performed += instance.OnFigure_1;
                @Figure_1.canceled += instance.OnFigure_1;
                @Figure_2.started += instance.OnFigure_2;
                @Figure_2.performed += instance.OnFigure_2;
                @Figure_2.canceled += instance.OnFigure_2;
                @ActivateRagdoll.started += instance.OnActivateRagdoll;
                @ActivateRagdoll.performed += instance.OnActivateRagdoll;
                @ActivateRagdoll.canceled += instance.OnActivateRagdoll;
                @Torque.started += instance.OnTorque;
                @Torque.performed += instance.OnTorque;
                @Torque.canceled += instance.OnTorque;
                @Join.started += instance.OnJoin;
                @Join.performed += instance.OnJoin;
                @Join.canceled += instance.OnJoin;
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnFigure_1(InputAction.CallbackContext context);
        void OnFigure_2(InputAction.CallbackContext context);
        void OnActivateRagdoll(InputAction.CallbackContext context);
        void OnTorque(InputAction.CallbackContext context);
        void OnJoin(InputAction.CallbackContext context);
        void OnMovement(InputAction.CallbackContext context);
    }
}
