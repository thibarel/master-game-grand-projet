using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitActionSwitch : MonoBehaviour
{
    [Space(20)]
    public GameObject Trap;
    [Space(20)]
    public Vector3 PositionA;
    public Quaternion RotationA;
    [Space(20)]
    public Vector3 PositionB;
    public Quaternion RotationB;
    [Space(20)]
    public float coolDown = 3;

    private void Start()
    {
        Trap.transform.position = PositionA;
        Trap.transform.rotation = RotationA;
    }

    float hitStamp = -100;
    private void OnTriggerEnter(Collider other)
    {
        if ((Time.time - hitStamp) < coolDown) return;

        if (other.tag == "Bullet" && Trap.transform.position == PositionB)
        {
            hitStamp = Time.time;
            Trap.transform.position = PositionA;
            Trap.transform.rotation = RotationA;
        }
        else if (other.tag == "Bullet" && Trap.transform.position == PositionA)
        {
            hitStamp = Time.time;
            Trap.transform.position = PositionB;
            Trap.transform.rotation = RotationB;
        }

    }

}
