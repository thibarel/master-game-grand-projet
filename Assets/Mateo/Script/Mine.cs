using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    public GameObject MineBody;
    public GameObject MineExplosion;
    [Space(20)]
    private Rigidbody rb;

    private float hitStamp = -100;
    public float coolDown = 3;
    [Space(20)]
    public int bumperForce;

    private void Start()
    {
        MineBody.SetActive(true);
        MineExplosion.SetActive(false);
    }

    private void Update()
    {
        if ((Time.time - hitStamp) > coolDown)
        {
            MineBody.SetActive(true);
            MineExplosion.SetActive(false);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if ((Time.time - hitStamp) < coolDown) return;

        rb = other.GetComponent<Rigidbody>();

        Explosion();
    }

    void Explosion()
    {
        rb.GetComponent<Rigidbody>().AddExplosionForce(bumperForce, transform.position, 1);

        MineBody.SetActive(false);
        MineExplosion.SetActive(true);

        hitStamp = Time.time;
    }
}
