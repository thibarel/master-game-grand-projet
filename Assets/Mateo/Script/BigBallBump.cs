using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBallBump : MonoBehaviour
{
    private Rigidbody rb;

    public int bumperForce;

    public void OnTriggerEnter(Collider other)
    {

        rb = other.GetComponent<Rigidbody>();

        rb.GetComponent<Rigidbody>().AddExplosionForce(bumperForce, transform.position, 1);

    }
}
