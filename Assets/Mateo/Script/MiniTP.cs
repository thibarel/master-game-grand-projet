using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniTP : MonoBehaviour
{
    public GameObject NewPos;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Bike")
        {
            other.transform.position = NewPos.transform.position;
        }
    }
}
