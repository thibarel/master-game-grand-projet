using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

// HitActionSwitch

[CustomEditor(typeof(HitActionSwitch)), CanEditMultipleObjects]
public class HitActionSwitchEditor : Editor
{
    protected virtual void OnSceneGUI()
    {
        HitActionSwitch example = (HitActionSwitch)target;

        EditorGUI.BeginChangeCheck();
        Vector3 newTargetPosition = Handles.PositionHandle(example.PositionA, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(example, "Change Look At Target Position");
            example.PositionA = newTargetPosition;
        }

        EditorGUI.BeginChangeCheck();
        newTargetPosition = Handles.PositionHandle(example.PositionB, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(example, "Change Look At Target Position");
            example.PositionB = newTargetPosition;
        }
    }
}
