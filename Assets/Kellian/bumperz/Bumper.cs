using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour
{

    public float explosionForce = 1500;


    public bool isMooving;

    public float upDistance = 1;
    public float downDistance = -1;

    public float speed = 1.19f;
    Vector3 pointA;
    Vector3 pointB;

    // Start is called before the first frame update
    void Start()
    {
        pointA = new Vector3(0, upDistance, 0);
        pointB = new Vector3(0, downDistance, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (isMooving == true)
        {
             float time = Mathf.PingPong(Time.time * speed, 1);
        transform.localPosition = Vector3.Lerp(pointA, pointB, time);
        }
        //PingPong between 0 and 1
       
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bike")
        {
            print("bump"); 
            other.GetComponent<Rigidbody>().AddExplosionForce(1500,transform.position,2);
        }

    }
}
