using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.VFX;
using MilkShake;



public class TestingHomie : MonoBehaviour
{
    public Gamepad gamepad;
    public Rigidbody rb;
    public float RotationForce;
    public float MaxRotationForce;
    public float AccelerationForce = 10;
    public float MaxSpeed = 1;
    public float dir_swap_speed = 10;
    public GameObject frontWheel;
    public GameObject rearWheel;
    public GameObject BikeSkin;
    public GameObject pilotSkin;
    public GameObject Precise_PilotSkin;
    public Animator PilotAnimator;
    public GameMaster gameMaster;
    public int PlayerID;
    public bool Activate_Bike;
    public float autoBrakeSpeed = 1;

    public InputMaster controls;

    public bool IsPerformingFigure = false;

    public bool Is_Dead = false;

    public Quaternion SavedRotation;
    bool HasRegisteredRotation;

    public float angleoffset;

    bool flipped;

    public float ScoreMultiplyer = 1;

    public float Bike_Integrity = 100;

    bool PostDying;

    public Vector3 angularvelocity;

    public GameObject bikeskeleton;

    public float meleeDMG = 5000;

    public GameObject SmokeObj;
    VisualEffect SmokeParticle;

    public GameObject Explosion_vfx;

    public List<GameObject> PropulsorVFXlist;

    public Material WhiteMat;
    public bool IsWhite;
    public float whitetimer = .1f;


    public float maxDistanceLeft = -29f;
    public float maxDistanceRight = 29f;


    Vector3 StoredVelocity;
    Vector3 StoredAngularVelocity;

    // pour faire le outline
    public GameObject Mesh;
    public GameObject Bmesh;

    [Header("Parametres du tir")]

    public GameObject newbullet;
    public Transform bulletSpawn;
    private float Fire = 0f;
    public int rateOfFire = 5;
    public float AvalableAmmo = 0;
    public int ammoCost = 5;
    public float MagSize = 50;
    public float shotPower = 50;
    public GameObject muzzleFlash;
    public float shoot_recoil = 5;

    public float meleeCd = .5f;

    public float lean_smoothingvalue = 0.2f;

    [SerializeField]

    public LayerMask ShootRaycast_LayersToIgnore;

    [SerializeField]
    public Gradient Ring_Blue_Gradient;
    public Gradient Ring_Red_Gradient;
    public Gradient Flame_Blue_Gradient;
    public Gradient Flame_Red_Gradient;

    [Header("Ammo VFX")]

    public VisualEffect Ammo_Effect;
    public Vector3 GuruPos;

    public VisualEffect FigureFX;

    [Header("CONTROLLER VALUE DEBUG")]

    public string GamepadName;
    public Vector2 Lstick;
    public Vector2 Rstick;
    public float TriggerTorque;

    public bool congratz = false;


    [Header("sound system")]

    public GameObject ShootSound;

    [Header("Sword Mode")]

    public bool SwordState;
    public GameObject SwordHand_Mesh;
    public GameObject SwordBack_Mesh;
    public GameObject SwordCollider;
    public VisualEffect SwordFx;


    private void Awake()
    {
      //  controls = new InputMaster();

    }
    // Start is called before the first frame update

    void Start()
    {
      
        rb = gameObject.GetComponent<Rigidbody>();
        PilotAnimator = pilotSkin.GetComponent<Animator>();
        SmokeParticle = SmokeObj.GetComponent<VisualEffect>();

        foreach (var item in PropulsorVFXlist)
        {
            if (PlayerID == 0)
            {
                item.GetComponent<VisualEffect>().SetGradient("Ring_Gradient", Ring_Red_Gradient);
                item.GetComponent<VisualEffect>().SetGradient("Flame_Gradient", Ring_Red_Gradient);
            }
            else { }

        }



    }


    // Update is called once per frame
    void Update()
    {

        if (gameObject.transform.position.z > 30)
        {
            gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, maxDistanceLeft);
        }

        if (gameObject.transform.position.z < -30)
        {
            gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, maxDistanceRight);
        }

        if (frontWheel.GetComponent<Groundchecker>().isgrounded == true || rearWheel.GetComponent<Groundchecker>().isgrounded == true) // fais tomber le joueur si il touche le sol pendant une figure
        {
            if (IsPerformingFigure == true)
            {
                pilotSkin.GetComponent<Pilot_RagdollController>().EnableRagdoll();
                IsPerformingFigure = false;
            }
        }




        CheckWhiteStatus();

        angularvelocity = rb.angularVelocity;
        meleeCd -= Time.deltaTime;

        if (gamepad != null)
        {


            if (gamepad.buttonWest.isPressed) // figure 1
            {
                Figure_01();
            }

            if (gamepad.buttonNorth.isPressed) // figure 2
            {
                Figure_02();
            }

            if (gamepad.buttonSouth.wasPressedThisFrame)
            {
                EnableSwordMode();
            }

            //if (gamepad.buttonEast.isPressed) // ragdoll
            //{
            //    pilotSkin.GetComponent<Pilot_RagdollController>().RagdollSwitch();
            //}

            if (gamepad.rightStickButton.isPressed/* && frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false*/)
            {
                Melee();
            }

        }

        if (Is_Dead == true && PostDying == false)
        {
            PostDyingScequence();

            PostDying = true;

        }

        Moove();
        Torque();

        LoopingChecker();

      


    }


    void OnMovement(InputValue value)
    {
        Lstick = value.Get<Vector2>();
    }

    public void Moove()
    {
        if (Activate_Bike == true && Is_Dead == false)
        {

            CalculateAimSpace();

            Vector3 RightRotation = new Vector3(BikeSkin.transform.localRotation.x, 180f, BikeSkin.transform.localRotation.y);

            Vector3 LeftRotation = new Vector3(BikeSkin.transform.localRotation.x, 0f, BikeSkin.transform.localRotation.y);

            Vector3 currentAngle = BikeSkin.transform.localEulerAngles;


            if (Lstick.x < -0.2f)
            {
                if (frontWheel.GetComponent<Groundchecker>().isgrounded == true || rearWheel.GetComponent<Groundchecker>().isgrounded == true)
                {
                    rb.AddRelativeForce(new Vector3(0, 0, (Lstick.x * AccelerationForce) * Time.deltaTime));

                    if (rb.velocity.z < 0)
                    {

                        print("slow");
                        currentAngle = new Vector3(
                 Mathf.LerpAngle(currentAngle.x, RightRotation.x, dir_swap_speed * Time.deltaTime),
                 Mathf.LerpAngle(currentAngle.y, RightRotation.y, dir_swap_speed * Time.deltaTime),
                 Mathf.LerpAngle(currentAngle.z, RightRotation.z, dir_swap_speed * Time.deltaTime));

                        BikeSkin.transform.localEulerAngles = currentAngle;


                    }
                    else
                    {
                        rb.AddRelativeForce(new Vector3(0, 0, (Lstick.x * AccelerationForce * 2) * Time.deltaTime));
                    }

                }



            }

            if (Lstick.x > 0.2f)
            {

                if (frontWheel.GetComponent<Groundchecker>().isgrounded == true || rearWheel.GetComponent<Groundchecker>().isgrounded == true)
                {
                    rb.AddRelativeForce(new Vector3(0, 0, (Lstick.x * AccelerationForce) * Time.deltaTime));


                    if (rb.velocity.z > 0)
                    {
                        currentAngle = new Vector3(
                     Mathf.LerpAngle(currentAngle.x, LeftRotation.x, dir_swap_speed * Time.deltaTime),
                     Mathf.LerpAngle(currentAngle.y, LeftRotation.y, dir_swap_speed * Time.deltaTime),
                     Mathf.LerpAngle(currentAngle.z, LeftRotation.z, dir_swap_speed * Time.deltaTime));

                        BikeSkin.transform.localEulerAngles = currentAngle;


                    }
                    else
                    {
                        rb.AddRelativeForce(new Vector3(0, 0, (Lstick.x * AccelerationForce * 2) * Time.deltaTime));
                    }

                }


            }

            if (Lstick.x < 0.2f && Lstick.x > -0.2f)
            {
                rb.velocity = Vector3.Lerp(rb.velocity, new Vector3(rb.velocity.x, rb.velocity.y, 0), autoBrakeSpeed * Time.deltaTime);
            }


            if (rb.velocity.magnitude > MaxSpeed)
            {
                rb.velocity = Vector3.ClampMagnitude(rb.velocity, MaxSpeed);
            }



        }
    }


    void OnTorque(InputValue value)
    {

        TriggerTorque = value.Get<float>();
        print(TriggerTorque);
       

    }

    public void Torque()
    {
        if (rb.velocity.z > 0)
        {
            float reversedTorqueValue = -TriggerTorque;
            pilotSkin.GetComponent<Animator>().SetFloat("trigger", (reversedTorqueValue));

        }
        else if (rb.velocity.z < 0)
        {



            pilotSkin.GetComponent<Animator>().SetFloat("trigger", (TriggerTorque));

        }

        if (TriggerTorque > 0)
        {
            rb.AddTorque(new Vector3((RotationForce * TriggerTorque) * Time.deltaTime, 0, 0));
        }

        if (TriggerTorque < 0)
        {
            rb.AddTorque(-new Vector3((RotationForce * -TriggerTorque) * Time.deltaTime, 0, 0));
        }




        if (TriggerTorque < 0.2 && TriggerTorque > -0.2)
        {
            var ActualTorque = rb.angularVelocity;
            var NegativeTorque = -ActualTorque;

            rb.AddTorque(NegativeTorque / 2);
        }

        rb.maxAngularVelocity = MaxRotationForce;

    }

    void Figure_01()
    {
        if (SwordState == false)
        {
            if (frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false && IsPerformingFigure == false)
            {

                PilotAnimator.SetTrigger("Figure01");
                FigureFX.SetFloat("Circle_Lifetime", 1);
                FigureFX.Play();
                IsPerformingFigure = true;

            }
        }
    }

    void Figure_02()
    {
        if (SwordState == false)
        {
            if (frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false && IsPerformingFigure == false)
            {

                PilotAnimator.SetTrigger("Figure02");
                FigureFX.SetFloat("Circle_Lifetime", 2);
                FigureFX.Play();
                IsPerformingFigure = true;
            }
        }
    }


    public void Figure_Hard_Reward()
    {
        AvalableAmmo += 10 * ScoreMultiplyer;
        ScoreMultiplyer += .5f;

        if (PlayerID == 0)
        {
            gameMaster.P1Multi.text = "x" + ScoreMultiplyer.ToString();

            gameMaster.P1Multi.fontSize = 70;
        }
        else
        {
            gameMaster.P2Multi.text = "x" + ScoreMultiplyer.ToString();
            gameMaster.P2Multi.fontSize = 70;
        }

        VisualEffect particleEffect = Instantiate(Ammo_Effect, transform.position, transform.rotation); // instantiation du vfx

        particleEffect.GetComponent<ParticlePositionner>().P_ID = PlayerID;



    }

    public void Figure_Easy_Reward()
    {
        AvalableAmmo += 5 * ScoreMultiplyer;
        ScoreMultiplyer += .5f;

        if (PlayerID == 0)
        {
            gameMaster.P1Multi.text = "x" + ScoreMultiplyer.ToString();
            gameMaster.P1Multi.fontSize = 70;
        }
        else
        {
            gameMaster.P2Multi.text = "x" + ScoreMultiplyer.ToString();
            gameMaster.P2Multi.fontSize = 70;
        }

        VisualEffect particleEffect = Instantiate(Ammo_Effect, transform.position, transform.rotation); // instantiation du vfx

        particleEffect.GetComponent<ParticlePositionner>().P_ID = PlayerID;
    }







    public void CalculateAimSpace()
    {
        Vector3 Axis = new Vector3(0, Rstick.y, Rstick.x);
        Vector3 Animvector = BikeSkin.transform.InverseTransformDirection(Axis);
        PilotAnimator.SetFloat("StickX", Animvector.z);
        PilotAnimator.SetFloat("StickY", Animvector.y);

        Shoot(Rstick.x, Rstick.y);
    }



    public void Initialise()
    {
        



    }


    void Shoot(float x, float y)
    {
        if (SwordState == false)
        {

            if (AvalableAmmo > MagSize)
            {

                AvalableAmmo = MagSize;

            }
            RaycastHit hitt;
            if (Physics.Raycast(transform.position, new Vector3(0, y, x), out hitt, Mathf.Infinity, LayerMask.GetMask("AimCollider")))
            {
                Debug.DrawRay(transform.position, new Vector3(0, y, x) * hitt.distance, Color.green);
                GameObject Target = hitt.transform.gameObject;


            }

            if (x < -.7f || x > .7f)
            {

                Fire += Time.deltaTime * 20;

                if (Fire > rateOfFire && AvalableAmmo > 0)
                {

                    RaycastHit hit;
                    // Does the ray intersect any objects excluding the player layer
                    if (Physics.Raycast(transform.position, new Vector3(0, y, x), out hit, Mathf.Infinity, LayerMask.GetMask("AimCollider")))
                    {
                        Debug.DrawRay(transform.position, new Vector3(0, y, x) * hit.distance, Color.green);
                        GameObject Target = hit.transform.gameObject;

                        AimedjoyAttack(Target);
                    }
                    else
                    {
                        Debug.DrawRay(transform.position, new Vector3(0, y, x) * 1000, Color.red);
                        joyAttack(x, y);
                    }

                    Fire = 0;
                }
            }

            if (y < -.7f || y > .7f)
            {

                Fire += Time.deltaTime * 20;

                if (Fire > rateOfFire && AvalableAmmo > 0)
                {
                    joyAttack(x, y);
                    Fire = 0;
                }
            }
            // un raycast qui si touche le aimassistcollider va faire un tir sp�cial en disant a la balle a son spawn de regarder l'autre joueur;


        }

    }

    void joyAttack(float x, float y)
    {

        if (IsPerformingFigure == false && Activate_Bike == true && Is_Dead == false)
        {

            muzzleFlash.GetComponent<VisualEffect>().Play();
            ShootSound.GetComponent<AudioSource>().Play();
            var instantied = Instantiate(newbullet, new Vector3(0, bulletSpawn.position.y, bulletSpawn.position.z), bulletSpawn.rotation);
            instantied.GetComponent<Bullet>().letzgoo(x, y, shotPower);
            instantied.GetComponent<Bullet>().Shooter = gameObject;
            instantied.GetComponent<Bullet>().PilotSkin = pilotSkin;
            AvalableAmmo -= ammoCost;
            gameMaster.TinyRumble(gamepad);
        }



    }

    void AimedjoyAttack(GameObject target)
    {

        if (IsPerformingFigure == false && Activate_Bike == true && Is_Dead == false)
        {

            muzzleFlash.GetComponent<VisualEffect>().Play();
            ShootSound.GetComponent<AudioSource>().Play();
            var instantied = Instantiate(newbullet, new Vector3(0, bulletSpawn.position.y, bulletSpawn.position.z), bulletSpawn.rotation);
            instantied.GetComponent<Bullet>().Shooter = gameObject;
            instantied.GetComponent<Bullet>().aimed = true;

            instantied.transform.LookAt(target.transform);
            instantied.GetComponent<Bullet>().PilotSkin = pilotSkin;
            AvalableAmmo -= ammoCost;
            gameMaster.TinyRumble(gamepad);
        }



    }


    void LoopingChecker()
    {
        if (frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false)
        {
            if (HasRegisteredRotation == false)
            {
                SavedRotation = transform.rotation;
                HasRegisteredRotation = true;
            }
            else
            {
                angleoffset = Quaternion.Angle(SavedRotation, transform.rotation);

                if (angleoffset > 175 && flipped == false)
                {
                    flipped = true;
                }

                if (angleoffset < 10 && flipped == true)
                {

                    LoopingReward();
                    flipped = false;
                    HasRegisteredRotation = false;
                }
            }



        }
        else
        {
            HasRegisteredRotation = false; flipped = false; ScoreMultiplyer = 1;
            if (PlayerID == 0)
            {
              

                var fontsizeRought = 70;
                var fontsizeLocked = Mathf.Clamp(fontsizeRought, 70, 210);

            
            }
            else
            {
                gameMaster.P2Multi.text = "x" + 1;

                var fontsizeRought = 70;
                var fontsizeLocked = Mathf.Clamp(fontsizeRought, 70, 210);

               
            }
        }

        void LoopingReward()
        {
          
        }
    }


    public void TakeDamages(float dmg)
    {
        SetWhite();
        float Pushforce = Random.Range(1, Bike_Integrity);

        if (Bike_Integrity < 100)
        {

            Bike_Integrity += dmg;


        }

        SmokeParticle.SetFloat("Spawnrate", Bike_Integrity);

    }


    public void PostDyingScequence()
    {
      
      
    }

    public void Melee()
    {

        if (SwordState == true)
        {

            if (meleeCd <= 0)
            {


                for (int i = 0; i < gameMaster.PlayerList.Count; i++)
                {
                    if (gameMaster.PlayerList[i].transform.gameObject != gameObject)
                    {
                        var dist = Vector3.Distance(gameObject.transform.position, gameMaster.PlayerList[i].transform.position);

                        Vector3 direction = (gameMaster.PlayerList[i].transform.position - gameObject.transform.position);

                        float AngleDirection = Vector3.Angle(Vector3.up, direction);

                        float ZValue = gameMaster.PlayerList[i].transform.position.z - gameObject.transform.position.z;

                        float angle = AngleDirection;

                        SwordFx.Reinit();
                        SwordFx.Play();
                        if (angle >= 0 && angle < 45)
                        {

                            PilotAnimator.SetTrigger("SwordUp");

                        }

                        if (angle > 135 && angle <= 180)
                        {


                            PilotAnimator.SetTrigger("SwordDown");


                        }

                        if (angle > 45 && angle <= 135)
                        {


                            if (ZValue > 0 && BikeSkin.transform.eulerAngles.y > 90)
                            {

                                PilotAnimator.SetTrigger("SwordBack");

                            }

                            if (ZValue > 0 && BikeSkin.transform.eulerAngles.y < 90)
                            {

                                PilotAnimator.SetTrigger("SwordFront");

                            }

                            if (ZValue < 0 && BikeSkin.transform.eulerAngles.y < 90)
                            {

                                PilotAnimator.SetTrigger("SwordBack");


                            }

                            if (ZValue < 0 && BikeSkin.transform.eulerAngles.y > 90)
                            {
                                PilotAnimator.SetTrigger("SwordFront");

                            }




                        }

                        if (dist <= 3 && gameMaster.PlayerList[i].GetComponent<BikeController>().Bike_Integrity >= 0)
                        {

                           

                        }

                    }


                }

                meleeCd = 2f;
            }

        }
    }


    public void SetWhite()
    {

        Precise_PilotSkin.GetComponent<Renderer>().material = WhiteMat;
        BikeSkin.GetComponentInChildren<Renderer>().material = WhiteMat;
        IsWhite = true;


    }

    public void UnsetWhite()
    {
        // unset 
        if (PlayerID == 1)
        {
            Precise_PilotSkin.GetComponent<Renderer>().material = gameMaster.BluePilot_Mat;
            BikeSkin.GetComponentInChildren<Renderer>().material = gameMaster.BlueBike_skin;
            IsWhite = false;
            whitetimer = 0.1f;
        }
        else
        {

            Precise_PilotSkin.GetComponent<Renderer>().material = gameMaster.RedPilot_Mat;
            BikeSkin.GetComponentInChildren<Renderer>().material = gameMaster.RedBike_skin;
            IsWhite = false;
            whitetimer = 0.1f;
        }

    }

    public void CheckWhiteStatus()
    {
        if (IsWhite == true)
        {
            whitetimer -= 1 * Time.deltaTime;
        }

        if (whitetimer <= 0)
        {

            UnsetWhite();

        }
    }

    public void EnableSwordMode()
    {
        if (SwordState == true)
        {
            SwordState = false;
            pilotSkin.GetComponent<Animator>().SetBool("SwordState", false);
            DisableSwordMesh();


        }
        else
        {
            SwordState = true;
            pilotSkin.GetComponent<Animator>().SetBool("SwordState", true);
           
        }
    }

    public void ActivateSwordMesh() // appel� par l'animation de draw de l'�p�e (en event, via le script pilotRagdollController)
    {
        SwordHand_Mesh.SetActive(true);
        SwordBack_Mesh.SetActive(false);
    }

    public void DisableSwordMesh()
    {
        SwordHand_Mesh.SetActive(false);
        SwordBack_Mesh.SetActive(true);
    }



    public void FreezePhysic(float time)
    {


        if (StoredVelocity == Vector3.zero && StoredAngularVelocity == Vector3.zero)
        {

            StoredVelocity = rb.velocity;
            StoredAngularVelocity = rb.angularVelocity;
            rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;

            Invoke("SetPhysicBack", time);
        }







    }

    public void SetPhysicBack()
    {

        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
        rb.velocity = StoredVelocity;
        rb.angularVelocity = StoredAngularVelocity;

        StoredVelocity = Vector3.zero;
        StoredAngularVelocity = Vector3.zero;
    }

}
