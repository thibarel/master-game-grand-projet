using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPitcherTest : MonoBehaviour
{


    public AudioSource Motor;

    public float Pitch = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        float Pitchclamped = Mathf.Clamp(Pitch,1,3);
        Motor.pitch = Pitchclamped;
    }
}
