%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: PlayerAvater
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/ElbowIK_L
    m_Weight: 1
  - m_Path: Armature/ElbowIK_L/ElbowIK_L_end
    m_Weight: 1
  - m_Path: Armature/ElbowIK_R
    m_Weight: 1
  - m_Path: Armature/ElbowIK_R/ElbowIK_R_end
    m_Weight: 1
  - m_Path: Armature/FootIk_L
    m_Weight: 1
  - m_Path: Armature/FootIk_L/FootIk_L_end
    m_Weight: 1
  - m_Path: Armature/FootIk_R
    m_Weight: 1
  - m_Path: Armature/FootIk_R/FootIk_R_end
    m_Weight: 1
  - m_Path: Armature/HandIK_L
    m_Weight: 1
  - m_Path: Armature/HandIK_L/HandIK_L_end
    m_Weight: 1
  - m_Path: Armature/HandIK_R
    m_Weight: 1
  - m_Path: Armature/HandIK_R/HandIK_R_end
    m_Weight: 1
  - m_Path: Armature/HeadTarget
    m_Weight: 1
  - m_Path: Armature/HeadTarget/HeadTarget_end
    m_Weight: 1
  - m_Path: Armature/KneeIK_L
    m_Weight: 1
  - m_Path: Armature/KneeIK_L/KneeIK_L_end
    m_Weight: 1
  - m_Path: Armature/KneeIK_R
    m_Weight: 1
  - m_Path: Armature/KneeIK_R/KneeIK_R_end
    m_Weight: 1
  - m_Path: Armature/Root
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_L/Leg_02_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_L/Leg_02_L/Foot_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_L/Leg_02_L/Foot_L/Toes_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_L/Leg_02_L/Foot_L/Toes_L/Toes_L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_R/Leg_02_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_R/Leg_02_R/Foot_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_R/Leg_02_R/Foot_R/Toes_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Leg_01_R/Leg_02_R/Foot_R/Toes_R/Toes_R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Index_01_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Index_01_L/Index_02_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Index_01_L/Index_02_L/Index_03_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Index_01_L/Index_02_L/Index_03_L/Index_03_L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Middle_01_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Middle_01_L/Middle_02_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Middle_01_L/Middle_02_L/Middle_03_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Middle_01_L/Middle_02_L/Middle_03_L/Middle_03_L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Pinky_01_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Pinky_01_L/Pinky_02_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Pinky_01_L/Pinky_02_L/Pinky_03_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Pinky_01_L/Pinky_02_L/Pinky_03_L/Pinky_03_L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Ring_01_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Ring_01_L/Ring_02_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Ring_01_L/Ring_02_L/Ring_03_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Ring_01_L/Ring_02_L/Ring_03_L/Ring_03_L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Thumb_01_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Thumb_01_L/Thumb_02_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Thumb_01_L/Thumb_02_L/Thumb_03_L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_L/Arm_L/Arm_01_L/Hand_L/Thumb_01_L/Thumb_02_L/Thumb_03_L/Thumb_03_L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Index_01_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Index_01_R/Index_02_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Index_01_R/Index_02_R/Index_03_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Index_01_R/Index_02_R/Index_03_R/Index_03_R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Middle_01_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Middle_01_R/Middle_02_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Middle_01_R/Middle_02_R/Middle_03_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Middle_01_R/Middle_02_R/Middle_03_R/Middle_03_R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Pinky_01_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Pinky_01_R/Pinky_02_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Pinky_01_R/Pinky_02_R/Pinky_03_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Pinky_01_R/Pinky_02_R/Pinky_03_R/Pinky_03_R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Ring_01_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Ring_01_R/Ring_02_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Ring_01_R/Ring_02_R/Ring_03_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Ring_01_R/Ring_02_R/Ring_03_R/Ring_03_R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Thumb_01_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Thumb_01_R/Thumb_02_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Thumb_01_R/Thumb_02_R/Thumb_03_R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Clavicle_R/Arm_R/Arm_01_R/Hand_R/Thumb_01_R/Thumb_02_R/Thumb_03_R/Thumb_03_R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Neck
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Spine_01/Spine_02/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: BodyMesh
    m_Weight: 1
  - m_Path: Sword_back
    m_Weight: 1
  - m_Path: Sword_InHand
    m_Weight: 1
