using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class laserPointer : MonoBehaviour
{

    public GameObject LaserFx;
    public GameObject LaserTarget;

    public Camera mainCam;

    public Vector3[] LaserPositions = new Vector3[2];

    public Vector3 AimPos = new Vector3(0,0,0);
    
    // Start is called before the first frame update
    void Start()
    {
        LaserPositions = new Vector3[2];
        LaserPositions[0] = new Vector3(0,0,0);
    }

    // Update is called once per frame
    void Update()
    {

       LaserTarget.transform.position = AimPos;
     

        LaserPositions[1] = LaserTarget.transform.localPosition;

        //float distance = Vector3.Distance (gameObject.transform.position, LaserTarget.transform.position);
        LaserFx.GetComponent<LineRenderer>().SetPositions(LaserPositions);
    }
}
