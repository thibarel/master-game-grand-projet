using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;


public class CONTROLLER_TESTING : MonoBehaviour
{

    Gamepad Player1Gamepad;
    Gamepad Player2Gamepad;


    public GameObject Cube_P1;
    public GameObject Cube_P2;

    public Vector2 p1_Lstick;

    public Vector2 p1_Rstick;

    public Vector2 p2_Lstick;

    public Vector2 p2_Rstick;

    public float p1_Ltrigger;

    public float p1_Rtrigger;

    public float p2_Ltrigger;

    public float p2_Rtrigger;



    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(string.Join("\n", Gamepad.all));
    }



    // Update is called once per frame
    void Update()
    {

        if (Player1Gamepad != null)
        {
            p1_Ltrigger = Player1Gamepad.leftTrigger.ReadValue();

            p1_Rtrigger = Player1Gamepad.rightTrigger.ReadValue();

            p1_Lstick = Player1Gamepad.leftStick.ReadValue();

            p1_Rstick = Player1Gamepad.rightStick.ReadValue();

            Cube_P1.transform.Translate(new Vector3(p1_Lstick.x, p1_Rstick.y, p1_Lstick.y) * Time.deltaTime);
        }

        if (Player2Gamepad != null)
        {
            p2_Lstick = Player2Gamepad.leftStick.ReadValue();

            p2_Rstick = Player2Gamepad.rightStick.ReadValue();

            p2_Ltrigger = Player2Gamepad.leftTrigger.ReadValue();

            p2_Rtrigger = Player2Gamepad.rightTrigger.ReadValue();

            Cube_P2.transform.Translate(new Vector3(p2_Lstick.x, p2_Rstick.y, p2_Lstick.y) * Time.deltaTime);
        }
        

       


        if (Gamepad.current.buttonSouth.wasPressedThisFrame)
        {
            if (Gamepad.current == Gamepad.all[0])
            {
                Player1Gamepad = Gamepad.current;
                print(Player1Gamepad.name + "Player 1");
            }

            if (Gamepad.current == Gamepad.all[1])
            {
                Player2Gamepad = Gamepad.current;
                print(Player2Gamepad.name + "Player 2");
            }

            // Rumble the left motor on the current gamepad slightly.
            //  Gamepad.current.SetMotorSpeeds(0.2f, 0.2f);
        }

 


    }

}
