using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Pilot_RagdollController : MonoBehaviour
{
    public InputMaster controls;
   
    public List<Collider> ColliderList;
    public List<Rigidbody> RbList;
    public Animator PlayerAnim ;
    public bool _ActivateRagdoll = true;
    GameObject OriginalParent;
    public GameObject Bike;

    public GameObject BodyMesh;

    float ragdollCD = 1;
    bool cdbool;

    float playerID;

    public Material RedPilotSkin;

    public Material BluePilotSkin;


    private void OnEnable()
    {
        
    }

    private void OnDisable()
    {
    }


    private void Awake()
    {
        controls = new InputMaster();
        
    }

    // Start is called before the first frame update
    void Start()
    {
        OriginalParent = transform.parent.gameObject;

        PlayerAnim = gameObject.GetComponent<Animator>();

        BodyMesh = Bike.GetComponent<BikeController>().pilotSkin;
        playerID = Bike.GetComponent<BikeController>().PlayerID;
        

        foreach (Collider col in gameObject.GetComponentsInChildren<Collider>())
        {
            ColliderList.Add(col);
        }

        foreach (Rigidbody rb in gameObject.GetComponentsInChildren<Rigidbody>())
        {
            RbList.Add(rb);
        }

        

        DisableRagdoll();
    }

    // Update is called once per frame
    void Update()
    {
        ragdollCD -= 1 * Time.deltaTime;

        if (ragdollCD < 0)
        {
            cdbool = false;
        }
        else
        {
            cdbool = true;
        }


    }


    public void RagdollSwitch()
    {
        if (cdbool == false)
        {
            _ActivateRagdoll = !_ActivateRagdoll;

            if (_ActivateRagdoll == true)
            {
                EnableRagdoll();
            }
            else { DisableRagdoll(); }

            ragdollCD = 1;
        }   
        
    }


     public void EnableRagdoll()
    {

        transform.parent = null;
        gameObject.transform.parent = null;
        
        for (var index = 0; index < ColliderList.Count; index++)
        {
            var colliderItem = ColliderList[index];
            colliderItem.isTrigger = false;
        }


        for (var index = 0; index < RbList.Count; index++)
        {
            var RbItem = RbList[index];
            RbItem.velocity = Bike.GetComponent<Rigidbody>().velocity;
            RbItem.isKinematic = false;
        }
        
        PlayerAnim.enabled = false;
        ExplosionForce();

        if(Bike.GetComponent<BikeController>() != null)
        {
            Bike.GetComponent<BikeController>().Is_Dead = true;
        }
        else { Bike.GetComponent < TestingHomie>().Is_Dead = true;}



        if(SceneManager.GetActiveScene().name == "NewMainMenu"){
            GameObject.Find("GameController").GetComponent<GameMaster>().Endtraining();
        } 

        if (playerID == 1)
        {
             BodyMesh.GetComponentInChildren<SkinnedMeshRenderer>().material = BluePilotSkin;
        }else{
                BodyMesh.GetComponentInChildren<SkinnedMeshRenderer>().material = RedPilotSkin;
        }

       
       
    }

    public void DisableRagdoll()
    {

        transform.parent = OriginalParent.transform;

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(Vector3.zero); 
        for (var index = 0; index < ColliderList.Count; index++)
        {
            var colliderItem = ColliderList[index];
            colliderItem.isTrigger = true;
        }

        for (var index = 0; index < RbList.Count; index++)
        {
            
            var RbItem = RbList[index];
            RbItem.velocity = Vector3.zero;
            RbItem.isKinematic = true;

           
        }

        PlayerAnim.enabled = true;

        if( Bike.GetComponent<BikeController>() != null)
        {
            Bike.GetComponent<BikeController>().Is_Dead = false; // ne garder que cette ligne si tu veux effacer les truc du testing homie (virer le if autour et le else du dessu)
        }
        else
        {
            Bike.GetComponent<TestingHomie>().Is_Dead = false;
        }
               
        

    }

    public void HardFigureEnding() // appel� par les animations a leur fin
    {   
        Bike.GetComponent<BikeController>().IsPerformingFigure = false; // signe la fin de la figure
        Bike.GetComponent<BikeController>().Figure_Hard_Reward();
    }

    public void EasyFigureEnding() // appel� par les animations a leur fin
    {
        Bike.GetComponent<BikeController>().IsPerformingFigure = false; // signe la fin de la figure
        Bike.GetComponent<BikeController>().Figure_Easy_Reward();
    }

  
    public void ExplosionForce()
    {

        for (var index = 0; index < RbList.Count; index++)
        {
            var RbItem = RbList[index];
            RbItem.AddExplosionForce(1500,Bike.transform.position, 2);
        }

        
    }

    //   public void ActivateSwordMesh()
    // {
    //     Bike.GetComponent<BikeController>().ActivateSwordMesh();
    // }


    
    public void ActivateEnergyWave(){
        print("trigger animevent");
    }


 


}
