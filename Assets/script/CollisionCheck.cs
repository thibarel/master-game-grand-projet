using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCheck : MonoBehaviour
{

    public Pilot_RagdollController Ragdollscript;


    private void Awake()
    {
        Ragdollscript = GetComponentInParent<Pilot_RagdollController>();
    }

    public void OnTriggerEnter(Collider other)
    {


        if(other.gameObject.tag == "Terrain")
        {
         
            //  Ragdollscript.EnableRagdoll();
           

            Vector3 forcepoz = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

            // raycast entre le joueur et la position du hit
            // récup de la normale avec le raycast
            // add force au point de connection
           

            Ragdollscript.Bike.GetComponent<BikeController>().environmentalDMG(forcepoz);

        }
       
        
    }

    public void OnTriggerStay(Collider other)
    {
         if(other.gameObject.tag == "Terrain")
        {
          
            //  Ragdollscript.EnableRagdoll();
           // GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            Vector3 forcepoz = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
           
            //GameObject Instantiatedsphere = Instantiate(sphere);
          //  Instantiatedsphere.transform.position = forcepoz;

            Ragdollscript.Bike.GetComponent<BikeController>().environmentalDMG(forcepoz);

        }
    }
}
