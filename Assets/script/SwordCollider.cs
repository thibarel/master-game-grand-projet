using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordCollider : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Owner;
    BikeController bikeController;
     GameObject OwnerBike;
    void Start()
    {
        bikeController = Owner.GetComponent<BikeController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnTriggerEnter(Collider other)
    {

        // si il touche la moto de l'ennemi, il l'explose,et il trigger le retrait du mode �p�e
        if (other.gameObject != Owner && other.transform.IsChildOf(Owner.transform) == false && other.gameObject.tag == "Bike")
        {
            print(other.gameObject.name);
            other.gameObject.GetComponent<BikeController>().pilotSkin.GetComponent<Pilot_RagdollController>().EnableRagdoll();

            Invoke("endOwnerSwordMode", 2);

        }


       


    }


    public void endOwnerSwordMode()
    {
       // Owner.GetComponent<BikeController>().EnableSwordMode();
    }
}
