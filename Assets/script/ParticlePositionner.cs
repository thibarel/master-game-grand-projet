using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class ParticlePositionner : MonoBehaviour
{

    public GameObject ParticleGuru;

    public float P_ID;

    public Vector3 RedPos;
    public Vector3 BluePos;

    public Gradient RedGradient;
    public Gradient BlueGradient;





    // Start is called before the first frame update
    void Start()
    {
        ParticleGuru.transform.parent = null;
        if (P_ID == 0)
        {
            ParticleGuru.transform.position = RedPos;

            gameObject.GetComponent<VisualEffect>().SetGradient("Color",RedGradient);
        }
        else
        {
            ParticleGuru.transform.position = BluePos;

            gameObject.GetComponent<VisualEffect>().SetGradient("Color", BlueGradient);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
