using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class MainMenu : MonoBehaviour
{

    

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;

        foreach ( Gamepad pad in Gamepad.all)
        {
            pad.SetMotorSpeeds(0, 0);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        SceneManager.LoadScene("LoadingScreen");

    }
}
