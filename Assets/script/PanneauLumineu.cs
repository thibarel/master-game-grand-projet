using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
public class PanneauLumineu : MonoBehaviour
{

    public GameObject panneau_principal;

    public GameObject light1;
    public GameObject light2;
    public GameObject light3;

    public float explosionforce;
    public float explosionRadius;

    public Vector3 explosionOffset;

    public bool Blow;

    public GameObject offsetempty;

    public Material red;
    public Material green;

    bool mission1;
    bool mission2;

    public VisualEffect explosion;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Blow == true){
            Training_FinisherMode();
            Blow = false;
        }


if(offsetempty != null){
     offsetempty.transform.position = panneau_principal.transform.position + explosionOffset;
}
   
    }


    public void Training_FinisherMode(){

        panneau_principal.GetComponent<Rigidbody>().isKinematic=false;
        explosion.Play();
        panneau_principal.GetComponent<Rigidbody>().AddExplosionForce(explosionforce,panneau_principal.transform.position + explosionOffset,explosionRadius);
        panneau_principal.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.Range(-150,150),Random.Range(-150,150),Random.Range(-150,150)));
        // pour chaque joueur dans la liste de joueurs faire takedamages 100% pour laisser activer le finisher pour lancer la partie
    }


    public void Mission1(){

        light1.GetComponent<MeshRenderer>().material = green;

    }

     public void Mission2(){

            light2.GetComponent<MeshRenderer>().material = green;
        
    }

     public void Mission3(){

            light3.GetComponent<MeshRenderer>().material = green;
        
    }
}
