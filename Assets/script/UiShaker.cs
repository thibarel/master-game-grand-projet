using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiShaker : MonoBehaviour
{

    public float RefreshRate = .5f;

    public float offset = 100;

    public float Duration = 1;

    public GameObject UiHeader;
    public bool IsRed;


    public float Redtimer;

    public Vector3 BasePos;

    public Color OriginalColor;





    // Start is called before the first frame update
    void Start()
    {
     
         BasePos = gameObject.transform.localPosition;

        StartCoroutine(Shaker());

        OriginalColor = UiHeader.GetComponent<Image>().color;
       
    }

    // Update is called once per frame
    void Update()
    {
        Duration -= Time.deltaTime;

        if (Duration > 0)
        {
            StartCoroutine(Shaker());
        }else{
            gameObject.transform.localPosition = BasePos;
        }

        CheckRedStatus();
    }


public void SetShakerTime( float time){

Duration = time;

}



    public IEnumerator Shaker( ){

        gameObject.transform.localPosition = BasePos + new Vector3( Random.Range(-offset,offset) , Random.Range(-offset,offset) , Random.Range(-offset,offset));


        yield return new WaitForSeconds(RefreshRate); 

        
    }


        public void SetRed()
    {
        if(IsRed == false)
        {
            // Precise_PilotSkin.GetComponent<Renderer>().material = RedMat;
            // BikeSkin.GetComponentInChildren<Renderer>().material = RedMat;
            UiHeader.GetComponent<Image>().color = Color.red;
            IsRed = true;

        }
      
    }

    public void UnsetRed()
    {
        // unset 
           UiHeader.GetComponent<Image>().color = OriginalColor;
           IsRed = false;
            Redtimer = 0.1f;
        
      

    }

    public void CheckRedStatus()
    {
        if (IsRed == true)
        {
            Redtimer -= 1 * Time.deltaTime;
        }

        if (Redtimer <= 0)
        {

            UnsetRed();

        }
    }
}
