using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{

    public bool isSelfDestroyed;
    public float cooldown = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        

        if(isSelfDestroyed == true)
        {
            cooldown -= Time.deltaTime;
            if(cooldown < 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
