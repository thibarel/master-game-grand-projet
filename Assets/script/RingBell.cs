using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingBell : MonoBehaviour
{

public GameMaster gm;

public CapsuleCollider RingbellCapsuleCollider;
public MeshCollider RingbellMeshCollider;

public float explosionforce;
public Vector3 explosionOffset;
public float explosionRadius;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameController").GetComponent<GameMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnTriggerEnter( Collider other){
        
        if (other.gameObject.tag == "Bullet")
        {

            gm.TrainingTargetShot(other.GetComponent<Rocket>().Shooter);

            gameObject.GetComponent<Rigidbody>().AddExplosionForce(explosionforce,other.transform.position + explosionOffset,explosionRadius);
            other.GetComponent<Rocket>().DestructionScequence();

            
        }

    }


    public void ItsFinisherTime(){

        RingbellMeshCollider.enabled= true;
        RingbellCapsuleCollider.enabled = false;
        gameObject.GetComponent<Rigidbody>().AddExplosionForce(1500,gameObject.transform.position + new Vector3(-.5f,0,0),1);
    }
     


}
