using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamCloseUp : MonoBehaviour
{

    public GameObject ObjectToFollow;
    public Vector3 Offset;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = ObjectToFollow.transform.transform.position + Offset;    
    }
}
