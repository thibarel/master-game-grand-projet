using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

public class Bullet : MonoBehaviour
{
    private float AutoDestroyer = 4;

    public float xx;
    public float yy;
    public float pwer;
    public float BulletDmg = 5;

    public float ImpactForce = 500;

    public GameObject Shooter;
    public GameObject PilotSkin;
    public GameObject metalHit_VFXobj;
    public GameObject DirtHit_VFXobj;

    public bool aimed;

    public void Start()
    {

        Material mymat = GetComponent<Renderer>().material;
        

        if (Shooter.GetComponent<BikeController>().PlayerID == 1)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.cyan;

            mymat.SetColor("_EmissionColor", Color.cyan);
        }
        else { gameObject.GetComponent<Renderer>().material.color = Color.red;

            mymat.SetColor("_EmissionColor", Color.red);
        }
    }

    private void FixedUpdate()
    {
        AutoDestroyer -= 1 * Time.deltaTime;
        if (AutoDestroyer < 0)
        {
            Destroy(gameObject);
        }
        if (aimed == false)
        {
            gameObject.GetComponent<Rigidbody>().velocity = (new Vector3(0, yy, xx) * pwer);
        }
        else
        {
            gameObject.GetComponent<Rigidbody>().velocity =  transform.forward * pwer;
        }
     
    }

    
    public void letzgoo( float x, float y, float shootPower)
    {
        xx = x;
        yy = y;
        pwer = shootPower;
    }




    public void OnTriggerEnter(Collider other)
    {

       
        if (other.gameObject.tag == "Terrain")
        {
            Destroy(gameObject);
        }


        

        
        if(other != null)
        {

            if (other != null && other.transform.IsChildOf(Shooter.transform) == false && Shooter != null && PilotSkin != null && other.gameObject != Shooter && other.gameObject != PilotSkin && other.gameObject.tag == "BikeSkin")
            {
                
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
               

                if(other.GetComponentInParent<BikeController>() != null)
                {
                    other.GetComponentInParent<BikeController>().TakeDamages(BulletDmg);

                    Vector3 forcedirection = (Shooter.transform.position - other.transform.position).normalized;
                    Vector3 FinalVector = -forcedirection * ImpactForce;

                    other.gameObject.GetComponentInParent<BikeController>().rb.AddForce(FinalVector * Time.deltaTime);
                    Instantiate(metalHit_VFXobj, other.transform.position + Vector3.up / 2 + new Vector3(1, 0, 0), other.transform.rotation);
                    Destroy(gameObject);
                }

              


                // ADD FORCE en fonction de l'angle tireur/receveur

               
               

            }
            else if (other.transform.IsChildOf(Shooter.transform) == false && other.gameObject != Shooter && other.gameObject != PilotSkin && other.gameObject.tag == "Bike")
            {
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
      
                other.GetComponent<BikeController>().TakeDamages(BulletDmg);
                Instantiate(metalHit_VFXobj, other.transform.position + Vector3.up / 2 + new Vector3(1, 0, 0), other.transform.rotation);

                // ADD FORCE en fonction de l'angle tireur/receveur

                Vector3 forcedirection = (Shooter.transform.position - other.transform.position).normalized;
                Vector3 FinalVector = -forcedirection * ImpactForce;

                other.gameObject.GetComponent<BikeController>().rb.AddForce(FinalVector * Time.deltaTime);

                Destroy(gameObject);

            }
        }
       


    }


    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Terrain")
        {
            Destroy(gameObject);
        }
    }

}
