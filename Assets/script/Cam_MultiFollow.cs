using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_MultiFollow : MonoBehaviour
{
    GameObject gameMaster;
    public GameObject MainCamera;
    Camera cam;

    public float BaseFOV = 37;
    public float MaxFOV = 70;

    public float MinFov = 0;

    float zdist;

    public List<Transform> targets;

    public List<float> Distances;

    public float CamMinHeight = 6;
    public float CamMaxHeight = 15;

    public float BiggestDistance;

    float fovOffset = 0;

    float Player1Distance;

    float Player2Distance;

    public bool iszooming;

    public GameObject CamShaker;
    
    Vector3 camoriginalPos;

    // Start is called before the first frame update
    void Start()
    {
        gameMaster = GameObject.Find("GameController");

        cam = MainCamera.GetComponent<Camera>();

        gameMaster.GetComponent<GameMaster>().CamList.Add(cam);

        camoriginalPos = cam.transform.localPosition; 

        // CamShaker = gameObject.GetComponentInChildren<Transform>().gameObject;

    }

    void Update()
    {

        foreach (GameObject PlayerInstance in GameObject.FindGameObjectsWithTag("Bike"))
        {
            if (!targets.Contains(PlayerInstance.transform))
            {

                targets.Add(PlayerInstance.transform);


            }

        }
    }


    // Update is called once per frame
    void LateUpdate()
    {
        if (!iszooming)
        {
            if (targets.Count != 0)
            {
                Vector3 CenterPoint = GetCenterPoint();
                Vector3 LocalizedCenterPoint = transform.InverseTransformDirection(CenterPoint);
                float clampedYpos = Mathf.Clamp(LocalizedCenterPoint.y, CamMinHeight, CamMaxHeight);
                transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(transform.localPosition.x, clampedYpos, transform.localPosition.z), 3*Time.deltaTime);
                
                cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, camoriginalPos, 3*Time.deltaTime) ;


                if (gameMaster.GetComponent<GameMaster>().Player1 != null)
                {

                    Player1Distance = Vector3.Distance(new Vector3(0, 0, 0), gameMaster.GetComponent<GameMaster>().Player1.transform.position);

                }
                else
                {
                    Player1Distance = 0;
                }

                if (gameMaster.GetComponent<GameMaster>().Player2 != null)
                {

                    Player2Distance = Vector3.Distance(new Vector3(0, 0, 0), gameMaster.GetComponent<GameMaster>().Player2.transform.position);

                }
                else
                {
                    Player2Distance = 0;
                }

                if (Player1Distance > Player2Distance)
                {
                    fovOffset = Player1Distance;
                }
                else
                {
                    fovOffset = Player2Distance;
                }


                float calcfov = BaseFOV + fovOffset;
                float Clamped_Fov = Mathf.Clamp(calcfov, MinFov, MaxFOV);           
                cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, Clamped_Fov, 3 * Time.deltaTime);


            }
        }



    }


    Vector3 GetCenterPoint()
    {
        if (targets.Count == 1)
        {
            return targets[0].position;
        }

        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i] != null)
            {
                bounds.Encapsulate(targets[i].position);
            }
            else
            {
                targets.Remove(targets[i]);
            }

        }

        return bounds.center;


    }


    public IEnumerator Shake(float duration, float magnitude)
    {


        Vector3 originalPos = CamShaker.transform.localPosition;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            float z = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            CamShaker.transform.localPosition = new Vector3(originalPos.x, y, z);

            elapsed += Time.deltaTime;

            yield return null;
        }

        CamShaker.transform.localPosition = originalPos;
    }


    public IEnumerator LookAtTarget(float duration, GameObject target)
    {
        GameObject cam = gameObject.GetComponentInChildren<Camera>().transform.gameObject;
        iszooming = true;
        print("heyy je lance le zoom sur la target :" + target.name + " pendant " + duration + " secondes");
        Vector3 originalPos = cam.transform.localPosition;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {

            print("zoom en cour !");

            Vector3 newpos = new Vector3(cam.transform.position.x, target.transform.position.y, target.transform.position.z);
            Vector3 transformtolocal = transform.InverseTransformPoint(newpos);

            cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, transformtolocal, 3 * Time.deltaTime);
            cam.GetComponent<Camera>().fieldOfView = Mathf.Lerp(cam.GetComponent<Camera>().fieldOfView, 12, 3 * Time.deltaTime);
            elapsed += Time.deltaTime;

            yield return null;
        }
        iszooming = false;
        // cam.transform.localPosition = originalPos;

    }







}