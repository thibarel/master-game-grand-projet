using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using MilkShake;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.VFX;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;



public class GameMaster : MonoBehaviour
{
    public Gamepad Player1Gamepad;
    public Gamepad Player2Gamepad;

    public GameObject mainCam;
    public GameObject Player_Prefab;

    public int PlayerCount;

    public List<string> gamepadlist;

    public List<GameObject> SpawnerList;

    public List<GameObject> PlayerList;

    public GameObject SpawnPlatform_1;

    public GameObject SpawnPlatform_2;

    public float Prewarm_time = 3;

    public TMP_Text Prewarm_Countdown;

    public bool started;

    public Material RedPilot_Mat;
    public Material BluePilot_Mat;

    public Material RedBike_skin;
    public Material BlueBike_skin;

    public float Player1Score = 0;
    public float Player2Score = 0;

    public bool Player1Spawned;
    public bool Player2Spawned;

    public GameObject Player1;
    public GameObject Player2;

    public TMP_Text TMP_P1Percent;
    public TMP_Text TMP_P2Percent;

    public bool endGame;

    public TMP_Text P1Multi;
    public TMP_Text P2Multi;

    public Color OutlineRed;
    public Color OutlineBlue;

    public Gradient FigureFx_Red;
    public Gradient FigureFx_Blue;


    [Header("Player Arrows")]

    public GameObject P1Arrow;

    public GameObject P2Arrow;

    public Vector3 ArrowOffset;


    [Header("Camera")]

    public Shaker myShaker;

    public ShakePreset ShakePreset;

    public List<Camera> CamList;

    [Header("Prematch pannel gestion")]

    public GameObject LeftPannel;
    public GameObject RightPannel;

    public bool L_Pannel_Active;
    public bool R_Pannel_Active;

    public GameObject Left_BlackPannel;
    public GameObject Right_BlackPannel;

    public GameObject GetReady_Txt;

    public GameObject RedPressARespawn;
    public GameObject BluePressARespawn;


    [Header("Pause Menu")]
    public bool IsPaused;
    public GameObject PauseHolder;
    public EventSystem eventSystem;
    public GameObject ActiveButton;

    [Header("Fireworks")]

    public List<GameObject> Fireworks_list;

    public Gradient RedFireworks_Gradient;
    public Gradient BlueFireworks_Gradient;

    public TMP_Text wintext;

    float choosenOne;


    [Header("Puppets")]

    public List<Sprite> BlueHappyPuppets;
    public List<Sprite> RedHappyPuppets;
    public List<Sprite> BlueSadPuppets;
    public List<Sprite> RedSadPuppets;

    public Image BluePuppet;
    public Image RedPuppet;

    public Image WinnerKingPuppet;
    public Image Foule_Left;
    public Image Foule_Right;

    public Image Left_Engrenage;
    public Image Right_Engrenage;
    public GameObject FouleSound;
    public GameObject StartMatchSound;
    public GameObject EndMatchSound;

    public GameObject countdownsound;

    public GameObject mainMusicSource;

    public AudioClip mainMenuMusic;
    public List<AudioClip> gameMusics;

    public Material RedOutlineMat;
    public Material BlueOutlineMat;



    public GameObject FinisherUI_Blue;
    public GameObject FinisherUI_Red;

    public Gradient FinisherGradient_Blue;
    public Gradient FinisherGradient_Red;

    public Material FinisherTrail_Blue;
    public Material FinisherTrail_Red;


    public GameObject RedTrainingReadyText;
    public GameObject BlueTrainingReadyText;

    public GameObject RedTrainingBoard;

    public GameObject BlueTrainingBoard;

    public bool Training_BlueComplete;
    public bool Training_RedComplete;

    public bool Training_RedMoveComplete;
    public bool Training_RedShootComplete;

    public bool Training_RedStuntComplete;

    public bool Training_BlueMoveComplete;
    public bool Training_BlueShootComplete;

    public bool Training_BlueStuntComplete;

    public bool trainingcompleted;



    public float blue_trainingScore;
    public float red_trainingScore;

    public Button TerrainChoice;


    public GameObject GlobalBlueUI;
    public GameObject GlobalRedUI;



    [Header("Keeper")]

    public List<GameObject> levelList;

    public GameObject levelSelectionPannel;

    public InformationKeeper infoKeeper;

    public GameObject postProcess;
    public Volume PostProcessVolume;
    public Vignette _Vignette;

    public bool vignette_darken;



    public List<GameObject> MapSelection_UiToHide;


    public GameObject blueAmmoGlobal;
    public GameObject redAmmoGlobal;

    public GameObject GameOver_MenuButton;

    public GameObject BlueCareAlert;

    public GameObject RedCareAlert;

    public GameObject YellowHeat;
    public GameObject PurpleHeat;

    public GameObject Finisher_UiAim_Red;
    public GameObject Finisher_UiAim_Blue;

    public float RedUltFill = 0;
    public float BlueUltFill = 0;


    public Vector3 finisherUIOffset;





    // Start is called before the first frame update
    void Start()
    {

        infoKeeper = GameObject.Find("InfoKeeper").GetComponent<InformationKeeper>(); // récupère les données globales
        mainCam = GameObject.Find("camholder");

        postProcess = GameObject.Find("PostProcessingComponent");
        PostProcessVolume = postProcess.GetComponent<Volume>();

        PostProcessVolume.profile.TryGet<Vignette>(out _Vignette);

        Time.timeScale = 1;

        gamepadlist.Clear();
        foreach (Gamepad gamepad in Gamepad.all)
        {
            gamepadlist.Add(gamepad.name);
        }

        if (Screen.width > 1920)
        {
            ArrowOffset = new Vector3(0, 170, 0);
            P1Arrow.transform.localScale = new Vector3(1, 1, 1);
            P2Arrow.transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            ArrowOffset = new Vector3(0, 80, 0);
            P1Arrow.transform.localScale = new Vector3(.5f, .5f, .5f);
            P2Arrow.transform.localScale = new Vector3(.5f, .5f, .5f);
        }

        if (SceneManager.GetActiveScene().name == "NewMainMenu")
        {

            started = true;
            mainMusicSource.GetComponent<AudioSource>().clip = mainMenuMusic;
            mainMusicSource.GetComponent<AudioSource>().Play();
        }

        if (SceneManager.GetActiveScene().name == "MainGame")
        {

            if (GameObject.Find("InfoKeeper") == null)
            {
               

                print("Default map loaded");


                Instantiate(levelList[0], Vector3.zero, transform.rotation);
            }
            else
            {
                 Instantiate(infoKeeper.SelectedTerrain, Vector3.zero, transform.rotation);
                Cam_MultiFollow camscript = mainCam.GetComponent<Cam_MultiFollow>();
                camscript.BaseFOV = infoKeeper.BaseFOV;
                camscript.MaxFOV = infoKeeper.MaxFov;
                camscript.MinFov = infoKeeper.MinFov;
                camscript.CamMinHeight = infoKeeper.MinHeight;
                camscript.CamMaxHeight = infoKeeper.MaxHeight;
            }

            StartMatchSound.GetComponent<AudioSource>().Play();
            mainMusicSource.GetComponent<AudioSource>().clip = gameMusics[Random.Range(0, gameMusics.Count)];

        }

        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }



    }



    // Update is called once per frame
    void Update()
    {

        RedUltFill += Time.deltaTime / 3;
        BlueUltFill += Time.deltaTime / 3;

        Finisher_UiAim_Blue.GetComponentInChildren<Image>().fillAmount = BlueUltFill;
        Finisher_UiAim_Red.GetComponentInChildren<Image>().fillAmount = RedUltFill;

        if (SceneManager.GetActiveScene().name == "NewMainMenu" && vignette_darken == true)
        {
            _Vignette.intensity.value = Mathf.Lerp(_Vignette.intensity.value, 1, Time.deltaTime);
            _Vignette.intensity.value = Mathf.Clamp(_Vignette.intensity.value, 0, 1);

            foreach (var item in MapSelection_UiToHide)
            {
                item.SetActive(false);
            }

        }
        else
        {

            if (SceneManager.GetActiveScene().name == "NewMainMenu")
            {

                _Vignette.intensity.value = Mathf.Lerp(_Vignette.intensity.value, 0.10f, Time.deltaTime);
                _Vignette.intensity.value = Mathf.Clamp(_Vignette.intensity.value, 0, 1);
                foreach (var item in MapSelection_UiToHide)
                {
                    item.SetActive(true);
                }

            }

        }

        if (SceneManager.GetActiveScene().name == "NewMainMenu" && Gamepad.current != null && Gamepad.current.selectButton.wasPressedThisFrame)
        {
            SkipTraining();

        }

        if (SceneManager.GetActiveScene().name == "NewMainMenu" && Gamepad.current != null && Gamepad.current.buttonEast.wasPressedThisFrame)
        {
            vignette_darken = false;
            levelSelectionPannel.SetActive(false);
        }

        if (Gamepad.current != null && Gamepad.current.buttonSouth.wasPressedThisFrame)
        {
            if (Gamepad.current == Gamepad.all[0] && Player1Spawned == false && Player2Score < 3)
            {
                Player1Gamepad = Gamepad.current;



                var newPlayer = Instantiate(Player_Prefab, transform.position, transform.rotation);
                Player1 = newPlayer;
                BikeController Bcontroller = newPlayer.GetComponent<BikeController>();

                Bcontroller.gamepad = Player1Gamepad;
                Bcontroller.Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().material = RedPilot_Mat;
                Bcontroller.BikeSkin.GetComponent<MeshRenderer>().material = RedBike_skin;

                //    Material[] pilotOutlined = Bcontroller.Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().materials;
                //    pilotOutlined[1] = RedOutlineMat;
                //    Bcontroller.Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().materials = pilotOutlined;

                //     Material[] outlined = Bcontroller.BikeSkin.GetComponent<MeshRenderer>().materials;
                //     outlined[1] = RedOutlineMat;
                //     Bcontroller.BikeSkin.GetComponent<MeshRenderer>().materials = outlined; 



                Bcontroller.PlayerTrail.material = FinisherTrail_Red;
                Bcontroller.FigureFX.SetGradient("Figure_Gradient", FigureFx_Red);
                Bcontroller.PlayerID = 0;
                UnlockPannel(0);
                SmallRumble(Player1Gamepad);
                Player1Spawned = true;

                PressARespawnTrigger(0, 2);

                if (started == true)
                {
                    newPlayer.GetComponent<BikeController>().Activate_Bike = true;
                }

            }

            if (Gamepad.current != null && Gamepad.current == Gamepad.all[1] && Player2Spawned == false && Player1Score < 3)
            {
                Player2Gamepad = Gamepad.current;



                var newPlayer = Instantiate(Player_Prefab, transform.position, transform.rotation);
                Player2 = newPlayer;
                BikeController Bcontroller = newPlayer.GetComponent<BikeController>();

                Bcontroller.gamepad = Player2Gamepad;
                Bcontroller.Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().material = BluePilot_Mat;
                Bcontroller.BikeSkin.GetComponent<MeshRenderer>().material = BlueBike_skin;

                // Bcontroller.Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().materials[1] = BlueOutlineMat;

                // Material[] pilotOutlined = Bcontroller.Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().materials;
                // pilotOutlined[1] = BlueOutlineMat;
                // Bcontroller.Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().materials = pilotOutlined;


                // Material[] outlined = Bcontroller.BikeSkin.GetComponent<MeshRenderer>().materials;
                // outlined[1] = BlueOutlineMat;
                // Bcontroller.BikeSkin.GetComponent<MeshRenderer>().materials = outlined; 





                Bcontroller.PlayerTrail.material = FinisherTrail_Blue;
                Bcontroller.FigureFX.SetGradient("Figure_Gradient", FigureFx_Blue);
                Bcontroller.PlayerID = 1;
                UnlockPannel(1);
                PressARespawnTrigger(1, 2);
                SmallRumble(Player2Gamepad);
                Player2Spawned = true;

                if (started == true)
                {
                    newPlayer.GetComponent<BikeController>().Activate_Bike = true;
                }
            }

            // Rumble the left motor on the current gamepad slightly.
            //  Gamepad.current.SetMotorSpeeds(0.2f, 0.2f);
        }

        if (PlayerCount == 2 && started == false)
        {
            Prewarm();

        }

        if (endGame && choosenOne == 2)
        {
            InfiniteFireworks(Player1);
        }

        if (endGame && choosenOne == 1)
        {

            InfiniteFireworks(Player2);


        }

        if (Player2Score >= 3 && endGame == false)
        {
            mainMusicSource.GetComponent<AudioSource>().Stop();
            Firework_GameEnd(Player2);
            wintext.text = "Victoire des violets !";
            wintext.color = new Color(103,23,229); // couleure violette
            EndMatchSound.GetComponent<AudioSource>().Play();
            WinnerKingPuppet.sprite = BlueHappyPuppets[Random.Range(0, BlueHappyPuppets.Count)];
            WinnerKingPuppet.GetComponent<Animator>().SetTrigger("anim");
            Foule_Left.GetComponent<Animator>().SetTrigger("ending");

            if (GameOver_MenuButton != null)
            {
                GameOver_MenuButton.SetActive(true);
            }

            Foule_Right.GetComponent<Animator>().SetTrigger("ending");
            choosenOne = 1;
            endGame = true;

            RedPressARespawn.SetActive(false);
        }

        if (Player1Score >= 3 && endGame == false)
        {
            mainMusicSource.GetComponent<AudioSource>().Stop();
            Firework_GameEnd(Player1);
            wintext.text = "Victoire des jaunes !";
            wintext.color = new Color(237,215,10); // couleure jaune
            EndMatchSound.GetComponent<AudioSource>().Play();
            WinnerKingPuppet.sprite = RedHappyPuppets[Random.Range(0, RedHappyPuppets.Count)];
            WinnerKingPuppet.GetComponent<Animator>().SetTrigger("anim");
            Foule_Left.GetComponent<Animator>().SetTrigger("ending");
            if (GameOver_MenuButton != null)
            {
                GameOver_MenuButton.SetActive(true);
            }
            Foule_Right.GetComponent<Animator>().SetTrigger("ending");
            choosenOne = 2;
            endGame = true;

            BluePressARespawn.SetActive(false);

        }



        if (Gamepad.current != null && Gamepad.current.startButton.wasPressedThisFrame && IsPaused == false)
        {
            Pause();
        }
        else if (Gamepad.current != null && Gamepad.current.startButton.wasPressedThisFrame && IsPaused == true)
        {
            StopPause();
        }

        CheckForFinisherUI();


    }

    private void LateUpdate()
    {
        PlayerPositionsOnCam();
    }

    public void Prewarm()
    {


        if (countdownsound.GetComponent<AudioSource>().isPlaying == false)
        {
            countdownsound.GetComponent<AudioSource>().Play();
        }
        Prewarm_time -= 1 * Time.deltaTime;
        Prewarm_Countdown.gameObject.SetActive(true);

        if (Prewarm_time > 1f)
        {
            Prewarm_Countdown.text = Prewarm_time.ToString("F0");
        }
        else
        {
            Prewarm_Countdown.text = "GO!";
        }


        if (Prewarm_time <= 0)
        {
            Prewarm_Countdown.gameObject.SetActive(false);
            StartRound();
            started = true;

        }
    }

    public void StartRound()
    {

        foreach (GameObject item in PlayerList)
        {
            item.GetComponent<BikeController>().Activate_Bike = true;


        }
        Firework_GameStart();
        Destroy(SpawnPlatform_1);
        Destroy(SpawnPlatform_2);
        mainMusicSource.GetComponent<AudioSource>().Play();
    }

    public void OnPlayerJoined()
    {
        print("Player Joined");
    }

    public void SmallRumble(Gamepad Gpd)
    {
        float time = .5f;

        Gpd.SetMotorSpeeds(0.5f, 0.5f);

        StartCoroutine(EndRumble(Gpd, time));

    }

    public void TinyRumble(Gamepad Gpd)
    {
        float time = .2f;

        Gpd.SetMotorSpeeds(0.3f, 0.3f);

        StartCoroutine(EndRumble(Gpd, time));
    }

    public void BigRumble(Gamepad Gpd)
    {
        float time = .5f;

        Gpd.SetMotorSpeeds(0.7f, 0.7f);

        StartCoroutine(EndRumble(Gpd, time));
    }

    IEnumerator EndRumble(Gamepad EndGamepad, float delay)
    {
        yield return new WaitForSeconds(delay);
        EndGamepad.SetMotorSpeeds(0.0f, 0.0f);
    }


    public void ResetSpecificPlayerValue()
    {

    }

    public void ResetPlayersValues()
    {
        foreach (GameObject go in PlayerList)
        {
            go.GetComponent<BikeController>().AvalableAmmo = 0;
            go.GetComponent<BikeController>().Bike_Integrity = 100;
            go.GetComponent<BikeController>().ScoreMultiplyer = 0;

        }


    }


    public void PlayerPositionsOnCam()
    {
        Camera Cam = mainCam.GetComponentInChildren<Camera>();


        if (Player1 != null)
        {
            Vector3 P1ScreenPos = Cam.WorldToScreenPoint(Player1.transform.position);
            P1Arrow.SetActive(true);
            P1Arrow.transform.position = P1ScreenPos + ArrowOffset;
            Finisher_UiAim_Red.transform.position = P1ScreenPos;

            FinisherUI_Red.transform.position = P1ScreenPos + finisherUIOffset;
        }
        else
        {
            P1Arrow.SetActive(false);
            Finisher_UiAim_Red.SetActive(false);
        }

        if (Player2 != null)
        {
            Vector3 P2ScreenPos = Cam.WorldToScreenPoint(Player2.transform.position);
            P2Arrow.SetActive(true);
            P2Arrow.transform.position = P2ScreenPos + ArrowOffset;
            Finisher_UiAim_Blue.transform.position = P2ScreenPos;

            FinisherUI_Blue.transform.position = P2ScreenPos + finisherUIOffset;

        }
        else
        {
            P2Arrow.SetActive(false);
            Finisher_UiAim_Blue.SetActive(false);
        }

    }


    public void OpenCheckPannels()
    {
        LeftPannel.GetComponent<Animator>().SetTrigger("Anim");
        RightPannel.GetComponent<Animator>().SetTrigger("Anim");
        GetReady_Txt.GetComponent<Animator>().SetTrigger("Anim");


    }

    public void UnlockPannel(float Pid)
    {

        if (SceneManager.GetActiveScene().name == "MainGame")
        {

            if (Pid == 0)
            {
                Left_BlackPannel.SetActive(false);
                L_Pannel_Active = true;

            }
            else
            {
                Right_BlackPannel.SetActive(false);
                R_Pannel_Active = true;
            }

            if (R_Pannel_Active && L_Pannel_Active)
            {
                OpenCheckPannels();
            }

        }


    }

    public void Firework_GameStart()
    {
        foreach (GameObject item in Fireworks_list)
        {

            item.SetActive(true);
        }
    }

    public void Firework_GameEnd(GameObject player)
    {

        if (player != null)
        {
            player.GetComponent<BikeController>().PilotAnimator.SetTrigger("win_idle");
        }



    }

    public void InfiniteFireworks(GameObject player)
    {
        foreach (GameObject item in Fireworks_list)
        {

            if (player = Player1)
            {
                item.GetComponent<VisualEffect>().SetGradient("Gradient", RedFireworks_Gradient);

                item.SetActive(true);

                item.GetComponent<VisualEffect>().Play();

                // faire un zoom sur le joueur gagnant

                // faire jouer une animation au joueur gagnant

                // afficher un texte de victoire et une UI pour quitter/recommencer


            }
            else
            {
                item.GetComponent<VisualEffect>().SetGradient("Gradient", BlueFireworks_Gradient);

                item.SetActive(true);

                item.GetComponent<VisualEffect>().Play();
            }

        }
    }

    public void Pause()
    {
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }
        Time.timeScale = 0;
        PauseHolder.SetActive(true);
        eventSystem.SetSelectedGameObject(ActiveButton);
    }

    public void StopPause()
    {
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }
        Time.timeScale = 1;
        PauseHolder.SetActive(false);

    }

    public void QuitGame()
    {
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }

        if (SceneManager.GetActiveScene().name == "NewMainMenu")
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene("NewMainMenu");
        }
    }

    public void RestartGame()
    {
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0, 0);
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }



    public void PuppetAction(float ActionType, float PlayerID)
    {


        FouleSound.GetComponent<AudioSource>().Play();
        if (PlayerID == 0)
        {

            if (ActionType == 1)
            {
                RedPuppet.sprite = (RedHappyPuppets[Random.Range(0, RedHappyPuppets.Count)]);
                BluePuppet.sprite = (BlueSadPuppets[Random.Range(0, BlueSadPuppets.Count)]);
            }
            if (ActionType == 2)
            {
                RedPuppet.sprite = (RedSadPuppets[Random.Range(0, RedSadPuppets.Count)]);
                BluePuppet.sprite = (BlueHappyPuppets[Random.Range(0, BlueHappyPuppets.Count)]);
            }

            RedPuppet.GetComponent<Animator>().SetTrigger("anim");
            BluePuppet.GetComponent<Animator>().SetTrigger("anim");

            var animspeedmodd = Random.Range(0, 150) / 100;

            var clampedspeed = Mathf.Clamp(animspeedmodd, 0.3f, 1);

            var animspeedmodd2 = Random.Range(0, 150) / 100;

            var clampedspeed2 = Mathf.Clamp(animspeedmodd2, 0.3f, 1);

            RedPuppet.GetComponent<Animator>().speed = clampedspeed;
            BluePuppet.GetComponent<Animator>().speed = clampedspeed2;
        }

        if (PlayerID == 1)
        {
            if (ActionType == 1)
            {
                RedPuppet.sprite = (RedSadPuppets[Random.Range(0, RedSadPuppets.Count)]);
                BluePuppet.sprite = (BlueHappyPuppets[Random.Range(0, BlueHappyPuppets.Count)]);
            }
            if (ActionType == 2)
            {
                RedPuppet.sprite = (RedHappyPuppets[Random.Range(0, RedHappyPuppets.Count)]);
                BluePuppet.sprite = (BlueSadPuppets[Random.Range(0, BlueSadPuppets.Count)]);
            }

            BluePuppet.GetComponent<Animator>().SetTrigger("anim");
            RedPuppet.GetComponent<Animator>().SetTrigger("anim");

            var animspeedmodd = Random.Range(0, 150) / 100;

            var clampedspeed = Mathf.Clamp(animspeedmodd, 0.3f, 1);

            var animspeedmodd2 = Random.Range(0, 150) / 100;

            var clampedspeed2 = Mathf.Clamp(animspeedmodd2, 0.3f, 1);

            RedPuppet.GetComponent<Animator>().speed = clampedspeed;
            BluePuppet.GetComponent<Animator>().speed = clampedspeed2;
        }

    }


    public void SoloPuppetAction(float ActionType, float PlayerID)
    {

        FouleSound.GetComponent<AudioSource>().Play();
        if (PlayerID == 0) //red puppet anim
        {

            if (ActionType == 1)
            {
                RedPuppet.sprite = (RedHappyPuppets[Random.Range(0, RedHappyPuppets.Count)]);

            }
            if (ActionType == 2)
            {
                RedPuppet.sprite = (RedSadPuppets[Random.Range(0, RedSadPuppets.Count)]);

            }

            RedPuppet.GetComponent<Animator>().SetTrigger("anim");


            var animspeedmodd = Random.Range(0, 150) / 100;
            var clampedspeed = Mathf.Clamp(animspeedmodd, 0.3f, 1);

            RedPuppet.GetComponent<Animator>().speed = clampedspeed;

        }

        if (PlayerID == 1) // blue puppet anim
        {
            if (ActionType == 1)
            {

                BluePuppet.sprite = (BlueHappyPuppets[Random.Range(0, BlueHappyPuppets.Count)]);
            }
            if (ActionType == 2)
            {

                BluePuppet.sprite = (BlueSadPuppets[Random.Range(0, BlueSadPuppets.Count)]);
            }

            BluePuppet.GetComponent<Animator>().SetTrigger("anim");


            var animspeedmodd = Random.Range(0, 150) / 100;
            var clampedspeed = Mathf.Clamp(animspeedmodd, 0.3f, 1);

            BluePuppet.GetComponent<Animator>().speed = clampedspeed;
        }
    }


    public void PressARespawnTrigger(float PlayerID, float Type)
    {
        if (PlayerID == 0)
        {
            if (Type == 1) // spawn
            {
                RedPressARespawn.SetActive(true);

            }

            if (Type == 2) // despawn
            {
                RedPressARespawn.SetActive(false);
            }
        }

        if (PlayerID == 1)
        {
            if (Type == 1)
            {
                BluePressARespawn.SetActive(true);
            }

            if (Type == 2)
            {
                BluePressARespawn.SetActive(false);
            }
        }
    }




    public void TrainingTargetShot(GameObject BulletOwner)
    {

        if (BulletOwner == Player1)
        {





            if (Training_RedShootComplete == false)
            {
                RedTrainingBoard.GetComponentInChildren<PanneauLumineu>().Mission3();
                Training_RedShootComplete = true;
            }

        }

        if (BulletOwner == Player2)
        {




            if (Training_BlueShootComplete == false)
            {
                BlueTrainingBoard.GetComponentInChildren<PanneauLumineu>().Mission3();
                Training_BlueShootComplete = true;
            }


        }

        TrainingStateChecker();
    }


    public void TrainingMovement(GameObject Owner)
    {

        if (Owner == Player1)
        {


            if (Training_RedMoveComplete == false)
            {
                RedTrainingBoard.GetComponentInChildren<PanneauLumineu>().Mission1();
                Training_RedMoveComplete = true;
            }

        }

        if (Owner == Player2)
        {



            if (Training_BlueMoveComplete == false)
            {
                BlueTrainingBoard.GetComponentInChildren<PanneauLumineu>().Mission1();
                Training_BlueMoveComplete = true;
            }

        }
        TrainingStateChecker();
    }

    public void TrainingFigure(GameObject Owner)
    {

        if (Owner == Player1)
        {



            if (Training_RedStuntComplete == false)
            {
                RedTrainingBoard.GetComponentInChildren<PanneauLumineu>().Mission2();
                Training_RedStuntComplete = true;
            }


        }

        if (Owner == Player2)
        {



            if (Training_BlueStuntComplete == false)
            {
                BlueTrainingBoard.GetComponentInChildren<PanneauLumineu>().Mission2();
                Training_BlueStuntComplete = true;
            }


        }
        TrainingStateChecker();
    }

    public void TrainingStateChecker()
    {


        if (
            Training_RedMoveComplete == true &&
            Training_RedShootComplete == true &&
            Training_RedStuntComplete == true &&
            Training_BlueMoveComplete == true &&
            Training_BlueShootComplete == true &&
            Training_BlueStuntComplete == true
        )
        {
            if (trainingcompleted == false)
            {
                BlueTrainingBoard.GetComponentInChildren<PanneauLumineu>().Blow = true;
                RedTrainingBoard.GetComponentInChildren<PanneauLumineu>().Blow = true;
                Player1.GetComponent<BikeController>().TakeDamages(100);
                Player2.GetComponent<BikeController>().TakeDamages(100);
                trainingcompleted = true;
            }


        }
    }


    public void SkipTraining()
    {

        levelSelectionPannel.SetActive(true);
        TerrainChoice.Select(); // séléctionne le bouton sur l'ui (besoin pour la manette)
        vignette_darken = true;



    }

    public void Endtraining()
    { // quand les joueurs ont finis l'entrainement et qu'ils se tuent avec l'ulti

        if (
               Training_RedMoveComplete == true &&
               Training_RedShootComplete == true &&
               Training_RedStuntComplete == true &&
               Training_BlueMoveComplete == true &&
               Training_BlueShootComplete == true &&
               Training_BlueStuntComplete == true
           )
        {
            trainingcompleted = true;
            Invoke("SkipTraining", 2);
        }


    }


    public void Terrain1Select()
    {

        infoKeeper = GameObject.Find("InfoKeeper").GetComponent<InformationKeeper>();

        infoKeeper.SelectedTerrain = levelList[0];
        infoKeeper.BaseFOV = 20;
        infoKeeper.MaxFov = 38;
        infoKeeper.MinFov = 0;
        infoKeeper.MinHeight = 7.8f;
        infoKeeper.MaxHeight = 15;


        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }


        SceneManager.LoadScene("LoadingScreen");

    }

    public void Terrain2Select()
    {

        infoKeeper = GameObject.Find("InfoKeeper").GetComponent<InformationKeeper>();

        infoKeeper.SelectedTerrain = levelList[1];
        infoKeeper.BaseFOV = 24;
        infoKeeper.MaxFov = 45;
        infoKeeper.MinFov = 37;
        infoKeeper.MinHeight = 14f;
        infoKeeper.MaxHeight = 25;
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }
        SceneManager.LoadScene("LoadingScreen");


    }

    public void Terrain3Select()
    {

        infoKeeper = GameObject.Find("InfoKeeper").GetComponent<InformationKeeper>();

        infoKeeper.SelectedTerrain = levelList[2];
        infoKeeper.BaseFOV = 24;
        infoKeeper.MaxFov = 47.5f;
        infoKeeper.MinFov = 37.6f;
        infoKeeper.MinHeight = 7.3f;
        infoKeeper.MaxHeight = 25;
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }
        SceneManager.LoadScene("LoadingScreen");


    }

    public void Terrain4Select()
    {

        infoKeeper = GameObject.Find("InfoKeeper").GetComponent<InformationKeeper>();

        infoKeeper.SelectedTerrain = levelList[3];
        infoKeeper.BaseFOV = 20;
        infoKeeper.MaxFov = 54.5f;
        infoKeeper.MinFov = 24;
        infoKeeper.MinHeight = 7;
        infoKeeper.MaxHeight = 25;
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }
        SceneManager.LoadScene("LoadingScreen");


    }

    public void Terrain5Select()
    {

        infoKeeper = GameObject.Find("InfoKeeper").GetComponent<InformationKeeper>();

        infoKeeper.SelectedTerrain = levelList[4];
        infoKeeper.BaseFOV = 20;
        infoKeeper.MaxFov = 41;
        infoKeeper.MinFov = 24;
        infoKeeper.MinHeight = 7;
        infoKeeper.MaxHeight = 25;
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }
        SceneManager.LoadScene("LoadingScreen");


    }

    public void Terrain6Select()
    {

        infoKeeper = GameObject.Find("InfoKeeper").GetComponent<InformationKeeper>();

        infoKeeper.SelectedTerrain = levelList[5];
        infoKeeper.BaseFOV = 38.2f;
        infoKeeper.MaxFov = 82.8f;
        infoKeeper.MinFov = 24;
        infoKeeper.MinHeight = 7;
        infoKeeper.MaxHeight = 25;
        foreach (Gamepad item in Gamepad.all)
        {
            item.SetMotorSpeeds(0f, 0f);
        }
        SceneManager.LoadScene("LoadingScreen");


    }


    public void GrowRedAmmo()
    {

        redAmmoGlobal.GetComponent<Grower>().Grow();


    }

    public void GrowBlueAmmo()
    {

        blueAmmoGlobal.GetComponent<Grower>().Grow();


    }

    public void CheckForFinisherUI()
    {
        if (Player1 != null && Player2 != null)
        {
            if (Player1.GetComponent<BikeController>().Bike_Integrity < 10)
            {
                RedCareAlert.SetActive(true);
                YellowHeat.SetActive(true);
            }
            else
            {
                RedCareAlert.SetActive(false);
                YellowHeat.SetActive(false);
            }

            if (Player1.GetComponent<BikeController>().Bike_Integrity < 10 && Player2.GetComponent<BikeController>().frontWheel.GetComponent<Groundchecker>().isgrounded == false && Player2.GetComponent<BikeController>().rearWheel.GetComponent<Groundchecker>().isgrounded == false)
            {
                FinisherUI_Blue.SetActive(true);

            }
            else
            {
                FinisherUI_Blue.SetActive(false);

            }

            if (Player2.GetComponent<BikeController>().Bike_Integrity < 10)
            {

                BlueCareAlert.SetActive(true);
                PurpleHeat.SetActive(true);
            }
            else
            {
                BlueCareAlert.SetActive(false);
                PurpleHeat.SetActive(false);
            }

            if (Player2.GetComponent<BikeController>().Bike_Integrity < 10 && Player1.GetComponent<BikeController>().frontWheel.GetComponent<Groundchecker>().isgrounded == false && Player1.GetComponent<BikeController>().rearWheel.GetComponent<Groundchecker>().isgrounded == false)
            {
                FinisherUI_Red.SetActive(true);

            }
            else
            {
                FinisherUI_Red.SetActive(false);

            }
        }
        else
        {
            FinisherUI_Red.SetActive(false);
            FinisherUI_Blue.SetActive(false);
            BlueCareAlert.SetActive(false);
            RedCareAlert.SetActive(false);
            YellowHeat.SetActive(false);
            PurpleHeat.SetActive(false);
        }
    }

    public void ShakeTheCam(float duration, float magnitude)
    {

        StartCoroutine(mainCam.GetComponent<Cam_MultiFollow>().Shake(duration, magnitude));


    }


    public IEnumerator SlowTime(float duration, float timeScale)
    {

        // initiate stuff i u need

        float elapsed = 0.0f;

        while (elapsed < duration)
        {

            //  do stuff here while time still run
            Time.timeScale = timeScale;
            Time.fixedDeltaTime = 0.02F * timeScale;

            elapsed += Time.deltaTime;

            yield return null;
        }

        // maybe do some more stuff here when loop is over ? 
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02F;
    }

}
