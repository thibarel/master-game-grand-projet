using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class HealthBar : MonoBehaviour
{
    // Start is called before the first frame update

    public Sprite RedHead;

    public Sprite BlueHead;

    public Image HeadBG;

    public Image[] HeadArray;

    public bool forPlayer1;

    public bool forPlayer2;

    GameMaster gameMaster;

    public Gradient HealthStatesGradient;

    
    [Header("AMMO BAR")]

    public Image AmmoJauge;
    public float MinValue;
    public float MaxValue;
    public float FillFollowSpeed = 2;

     [Header("AMMO BAR 3")]

     public List<GameObject> AmmoImagesList;



    [Header ("New UI")]

    public GameObject New_blueHpSlider;
    public GameObject New_RedHpSlider;

    public GameObject New_AmmoSlider;



    [Header("UI 3")]

    public GameObject PurpleFill;
    public GameObject YellowFill;



    void Start()
    {
        gameMaster = GameObject.Find("GameController").GetComponent<GameMaster>();  

     
    }

    // Update is called once per frame
    void Update()
    {
        WholeCalc();

    }


    void CalculateAmmo( float CurrentAmmo, float MaxAmmo)
    {
        float calc_Ammo = CurrentAmmo / MaxAmmo; // normalisation 
        float Output_ammo = calc_Ammo * (MaxValue - MinValue) + MinValue;

        New_AmmoSlider.GetComponent<Slider>().value = calc_Ammo;

        /// t'a une liste d'images a up ou pas
        /// t'a le nombre de balles actuelles du joueur
        ///

        for (int i = 0; i < AmmoImagesList.Count; i++)
        {
            if ( i < CurrentAmmo)
            {
                AmmoImagesList[i].SetActive(true);
            }else{
                AmmoImagesList[i].SetActive(false);
            }
        }
    }


    void CalculateDMG(BikeController Bcontroller)
    {
        if (Bcontroller != null)
        {

            float actualdmg = Bcontroller.Bike_Integrity;

            float maxdmg = 100f;

            float calc_dmg = actualdmg / maxdmg; // normalisation

            float calc_Percent = calc_dmg * 100;

            if(Bcontroller.PlayerID == 0)
            {
                
              //  gameMaster.TMP_P1Percent.SetText(calc_Percent.ToString() + "%") ;

                YellowFill.GetComponent<Image>().fillAmount = calc_dmg;

               // New_RedHpSlider.GetComponent<Slider>().value = calc_dmg;
   
            }
            else
            {
                PurpleFill.GetComponent<Image>().fillAmount = calc_dmg;

               // New_blueHpSlider.GetComponent<Slider>().value = calc_dmg;
            }

        }
    }



    public void WholeCalc()
    {
        if (forPlayer1)
        {
            for (int i = 0; i < HeadArray.Length; i++)
            {
                if (i <= gameMaster.Player1Score -1 )
                {
                    HeadArray[i].enabled = true;
                }
                else
                {
                    HeadArray[i].enabled = false;
                }
            }

            if(gameMaster.Player1 != null)
            {
                CalculateAmmo(gameMaster.Player1.GetComponent<BikeController>().AvalableAmmo, gameMaster.Player1.GetComponent<BikeController>().MagSize);
                CalculateDMG(gameMaster.Player1.GetComponent<BikeController>());
            }
            
        }

        if (forPlayer2)
        {
            for (int i = 0; i < HeadArray.Length; i++)
            {
                if (i <= gameMaster.Player2Score -1 )
                {
                    HeadArray[i].enabled = true;
                }
                else
                {
                    HeadArray[i].enabled = false;
                }
            }

            if (gameMaster.Player2 != null)
            {
                CalculateAmmo(gameMaster.Player2.GetComponent<BikeController>().AvalableAmmo, gameMaster.Player2.GetComponent<BikeController>().MagSize);
                CalculateDMG(gameMaster.Player2.GetComponent<BikeController>());
            }
          
        }
    }
}
