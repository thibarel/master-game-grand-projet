using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Rocket : MonoBehaviour
{
    private float AutoDestroyer = 4;

    public float xx;
    public float yy;
    public float pwer;
    public float BulletDmg = 5;

    public float ImpactForce = 500;

    public GameObject Shooter;

    public GameObject PilotSkin;
    public GameObject metalHit_VFXobj;
    public GameObject DirtHit_VFXobj;

    public VisualEffect Trail;

    public Gradient trailGradientRED;
    public Gradient trailGradientBLUE;

    public Material RedMat;
    public Material BlueMat;

    [ColorUsageAttribute(true, true)]
    public Color RedColor;

    public Color BlueColor;

    public Gradient RedGradient;

    public bool aimed;


    public VisualEffect ExplosionVFX;

    public float SmoothSpeed = 1;
    float smoothVelocity;

    float maxPwer;

    public float StartingScale = 0.2f;
    public float MaxScale = 1;

    public float scalespeed = 3;


    public float autoAimTimer = .1f;

    public LayerMask AimAssistLayer;

    public void Start()
    {

        Material mymat = GetComponentInChildren<Renderer>().material;

        Trail = GetComponentInChildren<VisualEffect>();

        if (Shooter.GetComponent<BikeController>().PlayerID == 1)
        {


            mymat.SetColor("_EmissionColor", BlueColor);
            Trail.SetGradient("trailGradient", trailGradientBLUE);
            gameObject.GetComponentInChildren<Renderer>().material = BlueMat;
        }
        else
        {

            mymat.SetColor("_EmissionColor", RedColor);
            Trail.SetGradient("trailGradient", trailGradientRED);
            gameObject.GetComponentInChildren<Renderer>().material = RedMat;
        }

        transform.LookAt(Shooter.transform.position + new Vector3(0, yy, xx) * 1000);
    }

    void Update()
    {
        autoAimTimer -= Time.deltaTime;
        gameObject.transform.Rotate(new Vector3(0,0,720) * Time.deltaTime,Space.Self);

        if (autoAimTimer < 0)
        {

            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 4, AimAssistLayer))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 4, Color.green);
             

                if (hit.transform.gameObject != null && hit.transform.gameObject != Shooter && hit.transform.tag == "Bike")
                {
                   // gameObject.transform.LookAt(hit.transform);
                    var targetRotation = Quaternion.LookRotation(hit.transform.position - transform.position);

                    // Smoothly rotate towards the target point.
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7 * Time.deltaTime);
                }
            }
            else
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 4 , Color.red);
             
            }

        }
        pwer = Mathf.SmoothDamp(pwer, maxPwer, ref smoothVelocity, SmoothSpeed);


        StartingScale += Time.deltaTime * scalespeed;
        float applyedScale = Mathf.Clamp(StartingScale, 0, MaxScale);

        transform.localScale = new Vector3(applyedScale, applyedScale, applyedScale);

    }

    private void FixedUpdate()
    {


        AutoDestroyer -= 1 * Time.deltaTime;
        if (AutoDestroyer < 0)
        {
            Trail.transform.parent = null;
            Trail.GetComponent<ParticleStayer>().EndParticle();
            Instantiate(ExplosionVFX, transform.position, Quaternion.Euler(0, 0, 0));
            Destroy(gameObject);
        }
        if (aimed == false)
        {
            gameObject.GetComponent<Rigidbody>().velocity = (transform.forward * pwer);

        }

    }


    public void letzgoo(float x, float y, float shootPower)
    {
        xx = x;
        yy = y;
        pwer = 0;
        maxPwer = shootPower;
    }


    public void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.tag == "Terrain" && Trail != null)
        {
            DestructionScequence();
        }





        if (other != null)
        {

            if (other != null && other.transform.IsChildOf(Shooter.transform) == false && Shooter != null && PilotSkin != null && other.gameObject != Shooter && other.gameObject != PilotSkin && other.gameObject.tag == "BikeSkin")
            {
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

                if (other.GetComponentInParent<BikeController>() != null)
                {
                    other.GetComponentInParent<BikeController>().TakeDamages(BulletDmg);

                    Instantiate(metalHit_VFXobj, other.transform.position + Vector3.up / 2 + new Vector3(1, 0, 0), other.transform.rotation);
                    DestructionScequence();
                }
            }
            else if (other.transform.IsChildOf(Shooter.transform) == false && other.gameObject != Shooter && other.gameObject != PilotSkin && other.gameObject.tag == "Bike")
            {
                gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                other.GetComponent<BikeController>().TakeDamages(BulletDmg);
                Instantiate(metalHit_VFXobj, other.transform.position + Vector3.up / 2 + new Vector3(1, 0, 0), other.transform.rotation);
                DestructionScequence();
            }
        }



    }


    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Terrain")
        {
            DestructionScequence();
        }
    }


    public void DestructionScequence()
    {
       
        if (Trail != null)
        {
            Trail.transform.parent = null;
            Trail.GetComponent<ParticleStayer>().EndParticle();
            VisualEffect xplosion = Instantiate(ExplosionVFX, transform.position, Quaternion.Euler(0, 0, 0));

            if (Shooter.GetComponent<BikeController>().PlayerID == 0)
            {
                xplosion.SetVector4("ColorFlat", RedColor);//
                xplosion.SetGradient("colorGradient", RedGradient);
            }

            Explosionforce();
           
            Destroy(gameObject);
        }
    }


    public void Explosionforce(){
        
        float radius = 5;
        float power = 200;

        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
          


            if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, 3.0F);
        }
    }

}
