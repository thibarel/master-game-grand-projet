using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class BrokenCar : MonoBehaviour
{
    // Start is called before the first frame update

    public List<GameObject> BikePartsList;
    public float explosionforce = 500;
    public float explosionRadius = 2;
    public GameObject explosionvfx;

    public GameObject ExplodeSound;

    public float timer = 5;

    public Material assignedskin; 



    void Start()
    {

        explosionvfx.transform.parent = null;

        explosionvfx.transform.rotation = new Quaternion(0, 0, 0, 0);


        for (int i = 0; i < BikePartsList.Count; i++)
        {
            BikePartsList[i].GetComponent<Rigidbody>().AddExplosionForce(explosionforce, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - .5f, gameObject.transform.position.z), explosionRadius);

            BikePartsList[i].GetComponent<MeshRenderer>().material = assignedskin;
        }

        ExplodeSound.GetComponent<AudioSource>().Play();

    }

    // Update is called once per frame
    void Update()
    {
        timer -= 1 * Time.deltaTime;

        if (timer <0)
        {
            for (int i = 0; i < BikePartsList.Count; i++)
            {
                Destroy(BikePartsList[i].transform.gameObject);
            }

            Destroy(gameObject);
        }
    }
}
