using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    public float RotationSpeed = 10;

    public bool flipped;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       

        if (flipped == false)
        {
            gameObject.transform.Rotate(new Vector3(RotationSpeed * Time.deltaTime, 0, 0));
        }
        else
        {
            gameObject.transform.Rotate(new Vector3(0, 0, RotationSpeed * Time.deltaTime));
        }
    }
}
