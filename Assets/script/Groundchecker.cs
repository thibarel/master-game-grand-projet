using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Groundchecker : MonoBehaviour
{
    /// <summary>
    /// Check la distance entre le sol et le propulseur et ajuste la position en appliquant une force
    /// restriction de la pouss�e quand la moto est en a�rien
    /// </summary>

    public float flyDistance; // distance d'�quilibrage de la moto par rapport au sol

    public GameObject Propulsor; // raccourcis de l'objet

    public GameObject Vehicle; // vehicule concern� par cette "roue"

    Rigidbody rb; // cach�, le rigidbody du v�hicule

    public bool isgrounded;

    public Vector3 Gcheck_hitpoint;

    [SerializeField] private LayerMask Raycast_LayersToIgnore;


    // Start is called before the first frame update
    void Start()
    {
        Propulsor = this.gameObject; // s'identifie a un propulseur
       
        rb = Vehicle.GetComponent<Rigidbody>(); // prends le rigidbody du vehicule


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        RaycastHit hit; // cr�ation de la variable ou on sauvegarde les donn�es du raycast ci dessous

        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, -transform.up, out hit, Mathf.Infinity, Raycast_LayersToIgnore)) // raycast( position de cet objet - direction du raycast - sauvegarde des donn�es de ce qui est touch� sous la variable "hit" - distance maxi du raycast (infini) - masque ignor�) 
        {
            Gcheck_hitpoint = hit.point;
            if (hit.distance < flyDistance +.5f)
            {
                isgrounded = true;
            }
            else { isgrounded = false; }

        }
    }
 
}
