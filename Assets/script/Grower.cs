using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grower : MonoBehaviour
{

    public GameObject stuffToGrow;
    public float HowMuchToGrow = 2;
    public float deflateSpeed = 1;
    Vector3 basescale;

    // Start is called before the first frame update
    void Start()
    {
      basescale = transform.localScale;
        Grow();
    }

    // Update is called once per frame
    void Update()
    {
      gameObject.transform.localScale = Vector3.Lerp(gameObject.transform.localScale, basescale, deflateSpeed * Time.deltaTime);

    }


    public void Grow(){

        gameObject.transform.localScale = transform.localScale + new Vector3(HowMuchToGrow,HowMuchToGrow,HowMuchToGrow);
        gameObject.GetComponentInChildren<Image>().color = Color.green;
        Invoke("BaseColor",.2f);

    }

    public void BaseColor(){
      gameObject.GetComponentInChildren<Image>().color = Color.white;
    }
}
