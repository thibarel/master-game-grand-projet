using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class LoadingController : MonoBehaviour
{

    public float timer = 5;
    public GameObject LoadingIcon;
    public GameObject PressAIcon;

    public Scene MainGame;

    // Start is called before the first frame update
    void Start()
    {

           foreach (Gamepad item in Gamepad.all)
        {
               item.SetMotorSpeeds(0f, 0f);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if (timer < 0)
        {
            LoadingIcon.SetActive(false);
            PressAIcon.SetActive(true);

            if (Gamepad.current.buttonSouth.wasPressedThisFrame)
            {
                SceneManager.LoadScene("MainGame");
            }
        }

    }
}
