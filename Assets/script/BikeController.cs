using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.VFX;
using MilkShake;
using UnityEngine.SceneManagement;
using UnityEngine.Animations.Rigging;


public class BikeController : MonoBehaviour
{

    public bool IsGodmode = false;
    public Gamepad gamepad;
    public Rigidbody rb;
    public float RotationForce;
    public float MaxRotationForce;
    public float AccelerationForce = 10;
    public float MaxSpeed = 1;
    public float dir_swap_speed = 10;
    public GameObject frontWheel;
    public GameObject rearWheel;
    public GameObject BikeSkin;
    public GameObject pilotSkin;
    public GameObject Precise_PilotSkin;
    public Animator PilotAnimator;
    public GameMaster gameMaster;
    public int PlayerID;
    public bool Activate_Bike;
    public float autoBrakeSpeed = 1;
    public float CounterBrakeSpeed = 1;

    public float leftMaxpos = 26;
    public float rightMaxpos = -26;

    public GameObject LeftBorder;
    public GameObject RightBorder;

    public InputMaster controls;

    public bool IsPerformingFigure = false;

    public bool Is_Dead = false;

    public Quaternion SavedRotation;
    bool HasRegisteredRotation;

    public float angleoffset;

    bool flipped;

    public float ScoreMultiplyer = 1;

    public float Bike_Integrity = 100;

    bool PostDying;

    public Vector3 angularvelocity;

    public GameObject bikeskeleton;

    public float meleeDMG = 5000;

    public GameObject SmokeObj;
    VisualEffect SmokeParticle;

    public GameObject Explosion_vfx;

    public List<GameObject> PropulsorVFXlist;

    public Material WhiteMat;
    public bool IsWhite;
    public float whitetimer = .1f;


    Vector3 StoredVelocity;
    Vector3 StoredAngularVelocity;

    // pour faire le outline
    public GameObject Mesh;
    public GameObject Bmesh;

    public float godmodtimer = .1f;

    [Header("Parametres du tir")]

    public GameObject Weapon;
    public GameObject newbullet;
    public Transform bulletSpawn;
    public float Fire = 0f;
    public int rateOfFire = 5;
    public float AvalableAmmo = 0;
    public int ammoCost = 1;
    public float MagSize = 10;
    public float shotPower = 50;
    public GameObject muzzleFlash;
    public float shoot_recoil = 5;

    public float meleeCd = .5f;

    public float lean_smoothingvalue = 0.2f;

    [SerializeField]

    public LayerMask ShootRaycast_LayersToIgnore;

    [SerializeField]
    public Gradient Ring_Blue_Gradient;
    public Gradient Ring_Red_Gradient;
    public Gradient Flame_Blue_Gradient;
    public Gradient Flame_Red_Gradient;

    [Header("Ammo VFX")]

    public VisualEffect Ammo_Effect;
    public Vector3 GuruPos;

    public VisualEffect FigureFX;

    [Header("CONTROLLER VALUE DEBUG")]

    public string GamepadName;
    public Vector2 Lstick;
    public Vector2 Rstick;
    public float Ltrigger;
    public float Rtrigger;

    public bool congratz = false;


    [Header("sound system")]

    public GameObject ShootSound;
    public GameObject MotorSound;

    public GameObject slashFX;
    public GameObject FigureSFX;

    public GameObject tpSFX;

    public GameObject spawnSFX;

    public GameObject FinishProcSfx;


    public GameObject DamageSFX;


    public float pitchspeed = 1;

    public float Pitch = 1;

    [Header("Sword Mode")]

    public bool finisherState;
    public GameObject SwordHand_Mesh;
    public GameObject SwordBack_Mesh;
    public GameObject SwordCollider;
    public GameObject SwordFx;
    public Camera SideCam;

    public TrailRenderer PlayerTrail;

    public GameObject plopper;

    public float MovementLoader = 0;
    public bool MovementLoaderDone = false;

    public bool isFreeze;

    public float jumpHeight = 1000;
    float JumpCD = 0.2f;


    public GameObject LaserPointer;
    bool isHandIk;
    public GameObject handIK;

    float FinisherTyming = 3;
    public float FinisherForce = 50;
    // public GameObject FinisherAimComponent;
    public bool isRushing = false;
    public float velocityMagnitude;


    public GameObject FinisherAura;
    public Color blueColor;
    public Color redColor;
    public float AuraSpeed = 0;
    public float rushTimer;

    Vector3 Finish_Velocity;
    float increasingSpeed = 0;

    public Material BlueOutline;
    public Material RedOutline;

    public AudioSource JumpAudio;

    public AudioSource BeginUltAudio;
    public AudioSource LaunchUltAudio;


    private void Awake()
    {
        controls = new InputMaster();

    }
    // Start is called before the first frame update

    void Start()
    {
        Initialise();
        rb = gameObject.GetComponent<Rigidbody>();
        PilotAnimator = pilotSkin.GetComponent<Animator>();
        SmokeParticle = SmokeObj.GetComponent<VisualEffect>();



        LeftBorder = GameObject.Find("LeftBorder");
        RightBorder = GameObject.Find("RightBorder");

        spawnSFX.GetComponent<AudioSource>().Play();

        foreach (var item in PropulsorVFXlist)
        {
            if (PlayerID == 0)
            {
                item.GetComponent<VisualEffect>().SetGradient("Ring_Gradient", Ring_Red_Gradient);
                item.GetComponent<VisualEffect>().SetGradient("Flame_Gradient", Ring_Red_Gradient);
                FinisherAura.GetComponent<Renderer>().material.SetColor("Color_24c83dc25fce4220979c5433edc7a064", redColor);

                Material[] pMatArray = Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().materials;
                pMatArray[1] = RedOutline;

                Precise_PilotSkin.GetComponent<SkinnedMeshRenderer>().materials = pMatArray;

                Material[] bMatArray = BikeSkin.GetComponent<MeshRenderer>().materials;
                bMatArray[1] = RedOutline;

                BikeSkin.GetComponent<MeshRenderer>().materials = bMatArray;

                Material[] wMatArray = Weapon.GetComponentInChildren<SkinnedMeshRenderer>().materials;

                wMatArray[2] = RedOutline;

                Weapon.GetComponentInChildren<SkinnedMeshRenderer>().materials = wMatArray;
            }
            else
            {


            }

        }

    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    // Update is called once per frame
    // void LateUpdate(){

    //     transform.localEulerAngles = new Vector3(transform.localEulerAngles.x,0,0);

    // }
    void Update()
    {
        velocityMagnitude = rb.velocity.magnitude;
        rb.angularVelocity = new Vector3(rb.angularVelocity.x, 0, 0);

        Fire += Time.deltaTime;
        meleeCd -= Time.deltaTime;
        JumpCD -= Time.deltaTime;

       

        if (gamepad != null)
        {
            Ltrigger = gamepad.leftTrigger.ReadValue();

            Rtrigger = gamepad.rightTrigger.ReadValue();

            Lstick = gamepad.leftStick.ReadValue();

            Rstick = gamepad.rightStick.ReadValue();



            if (gamepad.buttonWest.isPressed) // figure 1
            {
                Figure_01();
            }

            if (gamepad.buttonNorth.isPressed) // figure 2
            {
                Figure_02();
            }

            if (gamepad.buttonSouth.wasPressedThisFrame && JumpCD < 0) // jump input
            {
                Jump();
                JumpCD = 0.2f;
            }

            //if (gamepad.buttonEast.isPressed) // ragdoll
            //{
            //    pilotSkin.GetComponent<Pilot_RagdollController>().RagdollSwitch();
            //}

            if (gamepad.rightStickButton.isPressed)
            {
                Finisher();
            }

        }



        if (LeftBorder != null && gameObject.transform.position.z < LeftBorder.transform.position.z) // tp
        {
            tpSFX.GetComponent<AudioSource>().Play();
            GameObject plooopper = Instantiate(plopper, transform.position, transform.rotation);
            plooopper.transform.eulerAngles = new Vector3(90, 0, 0);

            gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, RightBorder.transform.position.z - 1);

            GameObject plooopper2 = Instantiate(plopper, transform.position, transform.rotation);
            plooopper2.transform.eulerAngles = new Vector3(-90, 0, 0);
        }

        if (RightBorder != null && gameObject.transform.position.z > RightBorder.transform.position.z) // tp
        {
            tpSFX.GetComponent<AudioSource>().Play();
            GameObject plooopper = Instantiate(plopper, transform.position, transform.rotation);
            plooopper.transform.eulerAngles = new Vector3(-90, 0, 0);

            gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, LeftBorder.transform.position.z + 1);

            GameObject plooopper2 = Instantiate(plopper, transform.position, transform.rotation);
            plooopper2.transform.eulerAngles = new Vector3(90, 0, 0);

        }


        CheckWhiteStatus();

        if (IsGodmode == true)
        {
            SetWhite();
        }


        if (congratz == false && ScoreMultiplyer >= 2.5)
        {
            gameMaster.SoloPuppetAction(1, PlayerID);

            congratz = true;
        }

        if (ScoreMultiplyer <= 1.5)
        {
            congratz = false;
        }

        RefreshEngineSound();

        // training mode only
        if (SceneManager.GetActiveScene().name == "NewMainMenu")
        {

            if (rb.velocity.z >= 7f || rb.velocity.z <= -7f)
            {

                MovementLoader += Time.deltaTime;

            }

            if (MovementLoader > 3 && MovementLoaderDone == false)
            {
                if (SceneManager.GetActiveScene().name == "NewMainMenu")
                {

                    gameMaster.GetComponent<GameMaster>().TrainingMovement(gameObject);

                }
                MovementLoaderDone = true;
            }

        }

        if (isRushing == true)
        {
            rushTimer -= Time.deltaTime;

            if (rushTimer > 0)
            {
                rb.velocity = Finish_Velocity;
            }
            else
            {
                EndFinisherMode(1);
            }


        }


        if (FinisherAura.activeInHierarchy)
        {


            increasingSpeed += 2 * Time.deltaTime;

            AuraSpeed += Time.deltaTime * increasingSpeed;



            FinisherAura.GetComponent<Renderer>().material.SetVector("Vector2_AuraWaveSpeed", new Vector3(0, AuraSpeed));

        }

        if (Activate_Bike == true && Is_Dead == false && finisherState == false)
        {

            CalculateAimSpace();
        }


        
    }
    void FixedUpdate()
    {
        angularvelocity = rb.angularVelocity; // debug

        Moove();
        LoopingChecker();
        CalculateModelRotation();
        if (Is_Dead == true && PostDying == false)
        {
            PostDyingScequence();

            PostDying = true;

        }

    }


    void Moove()
    {
        if (finisherState == false)

        {

            float TriggerCalc = Ltrigger + -Rtrigger;

            if (rb.velocity.z > 0)
            {


                pilotSkin.GetComponent<Animator>().SetFloat("trigger", (TriggerCalc));

            }
            else if (rb.velocity.z < 0)
            {

                pilotSkin.GetComponent<Animator>().SetFloat("trigger", (TriggerCalc));

            }


            if (Activate_Bike == true && Is_Dead == false && finisherState == false)
            {
                float torque = controls.Player.Torque.ReadValue<float>();



                if (Ltrigger > 0.2) // rotation droite
                {
                    // aide au retour à force de rotation zéro si déja engagé dans l'autre sens
                    if (rb.angularVelocity.x > 0)
                    {
                        rb.AddTorque(-new Vector3((RotationForce * 2 * Ltrigger) * Time.deltaTime, 0, 0), ForceMode.Acceleration);
                    }
                    else
                    { // force normale autrement
                        rb.AddTorque(-new Vector3((RotationForce * Ltrigger) * Time.deltaTime, 0, 0), ForceMode.Acceleration);
                    }
                }

                if (Rtrigger > 0.2) // rotation gauche
                {
                    if (rb.angularVelocity.x < 0)
                    {
                        rb.AddTorque(-new Vector3((RotationForce * 2 * -Rtrigger) * Time.deltaTime, 0, 0), ForceMode.Acceleration);
                    }
                    else
                    {
                        rb.AddTorque(-new Vector3((RotationForce * -Rtrigger) * Time.deltaTime, 0, 0), ForceMode.Acceleration);
                    }


                }

                if (Rtrigger < 0.2 && Ltrigger < 0.2) // autobrake de rotation
                {
                    var ActualTorque = rb.angularVelocity;
                    var NegativeTorque = -ActualTorque;

                    rb.AddTorque(NegativeTorque);
                }



                rb.maxAngularVelocity = MaxRotationForce;

                if (Lstick.x < -0.3f)
                {
                    if (frontWheel.GetComponent<Groundchecker>().isgrounded == true || rearWheel.GetComponent<Groundchecker>().isgrounded == true) // DEPLACEMENTS CLASSIQUES GAUCHE
                    {
                        rb.AddRelativeForce(new Vector3(0, 0, (Lstick.x * AccelerationForce) * Time.deltaTime), ForceMode.Acceleration);



                        if (rb.velocity.z > 0)
                        { // counterbrake

                            rb.AddRelativeForce(new Vector3(0, 0, (Lstick.x * AccelerationForce * 3) * Time.deltaTime), ForceMode.Acceleration);


                        }



                    }
                    else
                    { // AIR CONTROL GAUCHE

                        rb.AddForce(new Vector3(0, 0, Lstick.x * (AccelerationForce / 1.5f) * Time.deltaTime), ForceMode.Acceleration);

                    }




                }

                if (Lstick.x > 0.3f)
                {

                    if (frontWheel.GetComponent<Groundchecker>().isgrounded == true || rearWheel.GetComponent<Groundchecker>().isgrounded == true) // DEPLACEMENTS CLASSIQUES DROITE
                    {


                        rb.AddRelativeForce(new Vector3(0, 0, (Lstick.x * AccelerationForce) * Time.deltaTime), ForceMode.Acceleration);

                        if (rb.velocity.z < 0)
                        { // counterbrake

                            rb.AddRelativeForce(new Vector3(0, 0, (Lstick.x * AccelerationForce * 3) * Time.deltaTime), ForceMode.Acceleration);


                        }


                    }
                    else
                    { // AIR CONTROL DROITE

                        rb.AddForce(new Vector3(0, 0, Lstick.x * (AccelerationForce / 1.5f) * Time.deltaTime), ForceMode.Acceleration);

                    }




                } // droite

                if (Lstick.x < 0.3f && Lstick.x > -0.3f) // autobrake quand on touche plus a rien
                {
                    rb.velocity = Vector3.Lerp(rb.velocity, new Vector3(rb.velocity.x, rb.velocity.y, 0), autoBrakeSpeed * Time.deltaTime);



                }


                if (rb.velocity.magnitude > MaxSpeed)// clamp de max speed du joueur
                {
                    rb.velocity = Vector3.ClampMagnitude(rb.velocity, MaxSpeed);
                }

                if (frontWheel.GetComponent<Groundchecker>().isgrounded == true || rearWheel.GetComponent<Groundchecker>().isgrounded == true)
                {



                    if (IsPerformingFigure == true) // fais tomber le joueur si il touche le sol pendant une figure
                    {
                        IsPerformingFigure = false;
                        environmentalDMG(rb.transform.position + new Vector3(0, -1f, 0));
                        PilotAnimator.SetTrigger("CancelFigure");

                        // pilotSkin.GetComponent<Pilot_RagdollController>().EnableRagdoll();

                    }



                }


            }


        }
        else
        { // gestion de l'aim pendant le finisher

            FinisherTyming -= Time.deltaTime;



            if (Lstick.y > 0.5 || Lstick.y < -0.5 || Lstick.x > 0.5 || Lstick.x < -0.5)
            {

                // FinisherAimComponent.transform.LookAt(transform.position + new Vector3(0, Lstick.y, Lstick.x));

               // Vector3 aimToUI = gameMaster.mainCam.GetComponentInChildren<Camera>().WorldToScreenPoint(transform.position + new Vector3(0, Lstick.y, Lstick.x));

                if(PlayerID == 1){
                    gameMaster.Finisher_UiAim_Blue.transform.eulerAngles = new Vector3( 0, 0, (Mathf.Atan2(Lstick.y, Lstick.x) * 180 / Mathf.PI) - 90 );
                }else{
                    gameMaster.Finisher_UiAim_Red.transform.eulerAngles = new Vector3( 0, 0, (Mathf.Atan2(Lstick.y, Lstick.x) * 180 / Mathf.PI) - 90 );
                }

                if (BikeSkin.transform.localEulerAngles.y > 90)
                {
                    var targetRotation = Quaternion.LookRotation(transform.position + new Vector3(0, -Lstick.y, -Lstick.x) - transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 2 * Time.deltaTime);
                }
                else
                {
                    var targetRotation = Quaternion.LookRotation(transform.position + new Vector3(0, Lstick.y, Lstick.x) - transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 2 * Time.deltaTime);
                }

            }
            else
            {

                // FinisherAimComponent.transform.LookAt(transform.position + Vector3.forward);

                if(PlayerID == 1){
                    gameMaster.Finisher_UiAim_Blue.transform.eulerAngles = new Vector3( 0, 0, (Mathf.Atan2(0, 1) * 180 / Mathf.PI) - 90 );
                }else{
                    gameMaster.Finisher_UiAim_Red.transform.eulerAngles = new Vector3( 0, 0, (Mathf.Atan2(0,1) * 180 / Mathf.PI) - 90 );
                }

                if (BikeSkin.transform.localEulerAngles.y > 90)
                {
                    var targetRotation = Quaternion.LookRotation(transform.position + (-Vector3.forward) - transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 2 * Time.deltaTime);
                }
                else
                {
                    var targetRotation = Quaternion.LookRotation(transform.position + Vector3.forward - transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 2 * Time.deltaTime);
                }

            }

            if (FinisherTyming < 0)
            {
                rb.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionX;
                GoFinisher();
                FinisherTyming = 3;
            }
            else
            {
                Debug.DrawRay(transform.position, new Vector3(0, Lstick.y * 10, Lstick.x * 10), Color.green);
            }



        }


    }

    void GoFinisher()
    {
        // FinisherAimComponent.SetActive(false);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        LaunchUltAudio.Play();
        if (PlayerID == 1)
        {
            gameMaster.Finisher_UiAim_Blue.SetActive(false);
        }
        else
        {
            gameMaster.Finisher_UiAim_Red.SetActive(false);
        }

        if (Lstick.y > 0.3 || Lstick.y < -0.3 || Lstick.x > 0.3 || Lstick.x < -0.3)
        {
            Finish_Velocity = new Vector3(0, Lstick.y, Lstick.x) * FinisherForce;
        }
        else
        {
            Finish_Velocity = Vector3.forward * FinisherForce;
        }

        PlayerTrail.startWidth = 4;
        rushTimer = 1;
        MaxSpeed = 50;
        isRushing = true;



        // rb.AddForce(new Vector3(0,Rstick.y,Rstick.x) * FinisherForce, ForceMode.Impulse);
        //Invoke("EndFinisherMode",1);
    }

    void EndFinisherMode(float TypeOfEnding)
    {

        isRushing = false;
        FinisherAura.SetActive(false);
        MaxSpeed = 20;
        increasingSpeed = 0;
        AuraSpeed = 0.2f;
        finisherState = false;
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 0, 0);
        PlayerTrail.startWidth = 1;
        Invoke("endfinishAnim", 1);
        Weapon.GetComponent<Animator>().SetTrigger("RevertTransform");
        AvalableAmmo = 0;

        if (TypeOfEnding == 0)
        { // HIT FINISHER ENDING

            print("finisher has ended, hit");
            StartCoroutine(gameMaster.SlowTime(1, 0.2f));



        }

        if (TypeOfEnding == 1)
        { // MISSED FINISHER ENDING

            print("finisher has ended, misseed");

            foreach (GameObject playerInstance in gameMaster.PlayerList)
            {
                if (playerInstance != gameObject)
                {
                    playerInstance.GetComponent<BikeController>().Bike_Integrity += 20;
                }
            }


        }



    }



    void Figure_01()
    {
        if (finisherState == false)
        {
            if (frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false && IsPerformingFigure == false)
            {

                PilotAnimator.SetTrigger("Figure01");
                FigureFX.Reinit();
                FigureFX.SetFloat("Circle_Lifetime", 1);
                FigureFX.Play();
                IsPerformingFigure = true;

            }
        }
    }

    void Figure_02()
    {
        if (finisherState == false)
        {
            if (frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false && IsPerformingFigure == false)
            {
                PilotAnimator.SetTrigger("Figure02");
                FigureFX.Reinit();
                FigureFX.SetFloat("Circle_Lifetime", 2);
                FigureFX.Play();
                IsPerformingFigure = true;
            }
        }
    }


    public void Figure_Hard_Reward()
    {


        if (AvalableAmmo < MagSize)
        {
            FigureSFX.GetComponent<AudioSource>().Play();
            VisualEffect particleEffect = Instantiate(Ammo_Effect, transform.position, transform.rotation); // instantiation du vfx

            if (PlayerID == 1)
            {
                gameMaster.GrowBlueAmmo();

                particleEffect.SetGradient("Color", Ring_Blue_Gradient);
            }
            else
            {
                gameMaster.GrowRedAmmo();

                particleEffect.SetGradient("Color", Ring_Red_Gradient);
            }

            AvalableAmmo += 4 * ScoreMultiplyer;
            ScoreMultiplyer += .5f;






            if (SceneManager.GetActiveScene().name == "NewMainMenu")
            {
                gameMaster.GetComponent<GameMaster>().TrainingFigure(gameObject);
            }


        }

    }

    public void Figure_Easy_Reward()
    {

        if (AvalableAmmo < MagSize)
        {
            FigureSFX.GetComponent<AudioSource>().Play();
            VisualEffect particleEffect = Instantiate(Ammo_Effect, transform.position, transform.rotation); // instantiation du vfx

            if (PlayerID == 1)
            {
                gameMaster.GrowBlueAmmo();

                particleEffect.SetGradient("Color", Ring_Blue_Gradient);
            }
            else
            {
                gameMaster.GrowRedAmmo();

                particleEffect.SetGradient("Color", Ring_Red_Gradient);
            }


            AvalableAmmo += 2 * ScoreMultiplyer;
            ScoreMultiplyer += .5f;



            if (SceneManager.GetActiveScene().name == "NewMainMenu")
            {
                gameMaster.GetComponent<GameMaster>().TrainingFigure(gameObject);
            }

        }

    }

    void LoopingChecker()
    {
        if (frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false)
        {
            if (HasRegisteredRotation == false)
            {
                SavedRotation = transform.rotation;
                HasRegisteredRotation = true;
            }
            else
            {
                angleoffset = Quaternion.Angle(SavedRotation, transform.rotation);

                if (angleoffset > 165 && flipped == false)
                {
                    flipped = true;
                }

                if (angleoffset < 10 && flipped == true)
                {

                    LoopingReward();
                    flipped = false;
                    HasRegisteredRotation = false;
                }
            }



        }
        else
        {
            HasRegisteredRotation = false; flipped = false; ScoreMultiplyer = 1;
            if (PlayerID == 0)
            {
                //     gameMaster.P1Multi.text = "x" + 1;

                var fontsizeRought = 70;
                var fontsizeLocked = Mathf.Clamp(fontsizeRought, 70, 210);

                // gameMaster.P1Multi.fontSize = fontsizeLocked;
            }
            else
            {
                //                gameMaster.P2Multi.text = "x" + 1;

                var fontsizeRought = 70;
                var fontsizeLocked = Mathf.Clamp(fontsizeRought, 70, 210);

                //  gameMaster.P2Multi.fontSize = fontsizeLocked;
            }
        }

        void LoopingReward()
        {
            if (finisherState == false)
            {

                if (AvalableAmmo < MagSize)
                {
                    VisualEffect particleEffect = Instantiate(Ammo_Effect, transform.position, transform.rotation); // instantiation du vfx

                    if (PlayerID == 1)
                    {
                        gameMaster.GrowBlueAmmo();

                        particleEffect.SetGradient("Color", Ring_Blue_Gradient);
                    }
                    else
                    {
                        gameMaster.GrowRedAmmo();

                        particleEffect.SetGradient("Color", Ring_Red_Gradient);
                    }

                    if (SceneManager.GetActiveScene().name == "NewMainMenu")
                    {
                        gameMaster.GetComponent<GameMaster>().TrainingFigure(gameObject);
                    }

                    FigureSFX.GetComponent<AudioSource>().Play();
                    AvalableAmmo += 2 * ScoreMultiplyer;
                    ScoreMultiplyer += .5f;






                }
            }
        }
    }

    public void Initialise()
    {
        // quand le player pop lui donner une position de d�part et lui assigner d�finitivement un controller
        // update le nombre de joueurs dans le gamemanager et lui donner un controlleur en fontion

        gameMaster = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameMaster>();

        gameMaster.PlayerCount += 1;

        GamepadName = gamepad.name;

        gameObject.transform.position = gameMaster.SpawnerList[PlayerID].transform.position;


        if (PlayerID == 1)
        {
            //  BikeSkin.transform.eulerAngles += new Vector3 (0, 180, 0);

        }

        gameMaster.PlayerList.Add(gameObject);


    }


    public void CalculateAimSpace()
    {
        Vector3 Axis = new Vector3(0, Rstick.y, Rstick.x);
        Vector3 Animvector = BikeSkin.transform.InverseTransformDirection(Axis);
        Shoot(Rstick.x, Rstick.y);
        PilotAnimator.SetFloat("StickX", Animvector.z);
        PilotAnimator.SetFloat("StickY", Animvector.y);

    }


    void Shoot(float x, float y)
    {
        if (finisherState == false)
        {

            if (AvalableAmmo > MagSize) // sécurité pour la taille du chargeur
            {

                AvalableAmmo = MagSize;

            }



            if (x < -.6f || x > .6f || y < -.6f || y > .6f)
            {

                Vector3 calculatedAimPos = gameObject.transform.position + new Vector3(0, y, x) * 5;

                TwoBoneIKConstraint fuckthisikmethod = handIK.GetComponentInParent<TwoBoneIKConstraint>();
                fuckthisikmethod.data.targetPositionWeight = Mathf.Lerp(fuckthisikmethod.data.targetPositionWeight, 1, 20 * Time.deltaTime);
                fuckthisikmethod.data.targetRotationWeight = Mathf.Lerp(fuckthisikmethod.data.targetRotationWeight, 1, 20 * Time.deltaTime);
                var targetRotation = Quaternion.LookRotation(calculatedAimPos - transform.position);


                if (LaserPointer != null)
                {
                    LaserPointer.SetActive(true);
                    LaserPointer.GetComponent<laserPointer>().AimPos = Vector3.Lerp(LaserPointer.GetComponent<laserPointer>().AimPos, calculatedAimPos, 20 * Time.deltaTime);//calculatedAimPos;

                }
                //handIK.transform.LookAt(CalculatedLookatPos);
                handIK.transform.position = Vector3.Lerp(handIK.transform.position, calculatedAimPos, 20 * Time.deltaTime);
                handIK.transform.rotation = Quaternion.Slerp(handIK.transform.rotation, targetRotation, 20 * Time.deltaTime);

                if (Fire > rateOfFire && AvalableAmmo > 0)
                {
                    joyAttack(x, y);
                    Fire = 0;
                }
            }
            else
            {
                if (LaserPointer != null)
                {
                    TwoBoneIKConstraint fuckthisikmethod = handIK.GetComponentInParent<TwoBoneIKConstraint>();
                    fuckthisikmethod.data.targetPositionWeight = Mathf.Lerp(fuckthisikmethod.data.targetPositionWeight, 0, 20 * Time.deltaTime);
                    fuckthisikmethod.data.targetRotationWeight = Mathf.Lerp(fuckthisikmethod.data.targetRotationWeight, 0, 20 * Time.deltaTime);
                    LaserPointer.SetActive(false);
                    LaserPointer.GetComponent<laserPointer>().AimPos = LaserPointer.transform.position;
                }

            }




        }

    }


    void joyAttack(float x, float y)
    {

        if (IsPerformingFigure == false && Activate_Bike == true && Is_Dead == false && isFreeze == false)
        {

            muzzleFlash.GetComponent<VisualEffect>().Play();
            ShootSound.GetComponent<AudioSource>().Play();
            // var instantied = Instantiate(newbullet, new Vector3(0, bulletSpawn.position.y, bulletSpawn.position.z), Quaternion.Euler(0,0,0) );
            float clampY = Mathf.Clamp(y, -.2f, .2f);
            float clampx = Mathf.Clamp(x, -.2f, .2f);
            var instantied = Instantiate(newbullet, bulletSpawn.transform.position + new Vector3(0, clampY, clampx), Quaternion.Euler(0, 0, 0));

            instantied.GetComponent<Rocket>().letzgoo(x, y, shotPower);
            instantied.GetComponent<Rocket>().Shooter = gameObject;
            instantied.GetComponent<Rocket>().PilotSkin = pilotSkin;
            AvalableAmmo -= ammoCost;
            gameMaster.TinyRumble(gamepad);
        }



    }


    void OnAnimatorIK() /// WIP
    {

        if (isHandIk == true)
        {
            Debug.Log("activatehandik");


        }
        else
        {
            print("deactivatehandik");

        }

    }

    public void activatehandik()
    {

    }





    public void TakeDamages(float dmg)
    {

        SetWhite();

        DamageSFX.GetComponent<AudioSource>().Play();

        if (PlayerID == 1)
        {
            gameMaster.GlobalBlueUI.GetComponentInChildren<UiShaker>().SetShakerTime(0.2f);
            gameMaster.GlobalBlueUI.GetComponentInChildren<UiShaker>().SetRed();
        }
        else
        {
            gameMaster.GlobalRedUI.GetComponentInChildren<UiShaker>().SetShakerTime(0.2f);
            gameMaster.GlobalRedUI.GetComponentInChildren<UiShaker>().SetRed();
        }


        if (Bike_Integrity > 0)
        {

            Bike_Integrity -= dmg;


        }


        if (Bike_Integrity < 20)
        {

            SmokeParticle.SetFloat("Spawnrate", 100);
        }

        if (Bike_Integrity < 50)
        {

            SmokeParticle.SetFloat("Spawnrate", 80);
        }

        if (Bike_Integrity < 75)
        {

            SmokeParticle.SetFloat("Spawnrate", 35);
        }

        if (Bike_Integrity < 90)
        {

            SmokeParticle.SetFloat("Spawnrate", 10);
        }
    }


    public void PostDyingScequence()
    {
        gameMaster.mainCam.GetComponent<Cam_MultiFollow>().targets.Remove(transform);
        gameMaster.ShakeTheCam(0.7f, 0.1f);
        gameMaster.ResetPlayersValues();
        gameMaster.BigRumble(gamepad);



        gameMaster.PuppetAction(2, PlayerID);



        gameMaster.PressARespawnTrigger(PlayerID, 1);

        if (gamepad == gameMaster.Player1Gamepad)
        {
            gamepad = null;

            var dedbike = Instantiate(Explosion_vfx, gameObject.transform.position + new Vector3(1, 0, 0), gameObject.transform.rotation);

            dedbike.GetComponent<BrokenCar>().assignedskin = gameMaster.GetComponent<GameMaster>().RedBike_skin;

            gameMaster.PlayerCount -= 1;

            if (SceneManager.GetActiveScene().name == "MainGame")
            {

                gameMaster.Player2Score += 1;

            }

            // gameMaster.P1Multi.text = "x1";

            // gameMaster.P1Multi.fontSize = 70;

            gameMaster.Player1Spawned = false;

            gameMaster.PlayerList.Remove(gameObject);



            Destroy(gameObject);
        }

        if (gamepad == gameMaster.Player2Gamepad)
        {
            gamepad = null;


            var dedbike = Instantiate(Explosion_vfx, gameObject.transform.position + new Vector3(1, 0, 0), gameObject.transform.rotation);

            dedbike.GetComponent<BrokenCar>().assignedskin = gameMaster.GetComponent<GameMaster>().BlueBike_skin;
            gameMaster.PlayerCount -= 1;

            if (SceneManager.GetActiveScene().name == "MainGame")
            {

                gameMaster.Player1Score += 1;

            }



            // gameMaster.P2Multi.text = "x1";

            //gameMaster.P2Multi.fontSize = 70;

            gameMaster.Player2Spawned = false;

            gameMaster.PlayerList.Remove(gameObject);

            Destroy(gameObject);
        }

    }

    public void Finisher()
    {

        // faire s'arreter le joueur en l'air
        // faire lookat le joueur dans la direction de visée
        // lancer le joueur dans la direction de visée après 3 secondes

        if (!finisherState && frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false)
        {
            /*si l'autre joueur est au dessus de 90 pourcent de d�gats subis permettre lulti*/
            for (int i2 = 0; i2 < gameMaster.PlayerList.Count; i2++)
            {
                if (gameMaster.PlayerList[i2].transform.gameObject != gameObject)
                {
                    if (gameMaster.PlayerList[i2].GetComponent<BikeController>().Bike_Integrity <= 5)
                    {
                        pilotSkin.GetComponent<Animator>().SetBool("SwordState", true);

                        Weapon.GetComponent<Animator>().SetTrigger("StartTransform");
                        gameMaster.ShakeTheCam(3f, 0.1f);
                        BeginUltAudio.Play();
                        StartCoroutine(gameMaster.mainCam.GetComponent<Cam_MultiFollow>().LookAtTarget(1, gameObject));

                        // FinisherAimComponent.SetActive(true);
                        
                        if (PlayerID == 1)
                        {
                            gameMaster.BlueUltFill = 0;
                            gameMaster.Finisher_UiAim_Blue.SetActive(true);
                        }else{
                            gameMaster.RedUltFill = 0;
                            gameMaster.Finisher_UiAim_Red.SetActive(true);
                        }
                        

                        rb.constraints = RigidbodyConstraints.FreezePosition;
                        rb.angularVelocity = Vector3.zero;
                        finisherState = true;
                        FinisherTyming = 3; // durée de visée avant le départ en ligne droite

                        AuraSpeed = 0.2f;




                    }
                }
            }
        }


    }


    public void SetWhite()
    {
        if (IsWhite == false)
        {
            Precise_PilotSkin.GetComponent<Renderer>().material = WhiteMat;
            BikeSkin.GetComponentInChildren<Renderer>().material = WhiteMat;

            IsWhite = true;

        }

    }

    public void UnsetWhite()
    {
        // unset 
        if (PlayerID == 1)
        {
            Precise_PilotSkin.GetComponent<Renderer>().material = gameMaster.BluePilot_Mat;
            BikeSkin.GetComponentInChildren<Renderer>().material = gameMaster.BlueBike_skin;
            IsWhite = false;
            whitetimer = 0.2f;
        }
        else
        {

            Precise_PilotSkin.GetComponent<Renderer>().material = gameMaster.RedPilot_Mat;
            BikeSkin.GetComponentInChildren<Renderer>().material = gameMaster.RedBike_skin;
            IsWhite = false;
            whitetimer = 0.2f;
        }

    }

    public void CheckWhiteStatus()
    {
        if (IsWhite == true)
        {
            whitetimer -= 1 * Time.deltaTime;
        }

        if (whitetimer <= 0)
        {

            UnsetWhite();

        }
    }

    // public void EnableSwordMode()
    // {
    //     if (finisherState == true)
    //     {


    //         finisherState = false;
    //         pilotSkin.GetComponent<Animator>().SetBool("finisherState", false);
    //         DisableSwordMesh();
    //         SwordFx.SetActive(false);

    //     }
    //     else
    //     {
    //         finisherState = true;
    //         SwordFx.SetActive(true);

    //         FinishProcSfx.GetComponent<AudioSource>().Play();
    //         pilotSkin.GetComponent<Animator>().SetBool("finisherState", true);
    //         gameMaster.mainCam.GetComponentInParent<Cam_MultiFollow>().SwitchCam(SideCam, 1.5f, true);

    //     }
    // }


    // public void ActivateSwordMesh() // appel� par l'animation de draw de l'�p�e (en event, via le script pilotRagdollController)
    // {
    //     SwordHand_Mesh.SetActive(true);
    //     SwordBack_Mesh.SetActive(false);
    // }

    // public void DisableSwordMesh()
    // {
    //     SwordHand_Mesh.SetActive(false);
    //     SwordBack_Mesh.SetActive(true);

    // }



    public void FreezePhysic(float time)
    {

        if (StoredVelocity == Vector3.zero && StoredAngularVelocity == Vector3.zero)
        {

            StoredVelocity = rb.velocity;
            StoredAngularVelocity = rb.angularVelocity;
            rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
            isFreeze = true;
            Invoke("SetPhysicBack", time);
        }

    }

    public void SetPhysicBack()
    {

        rb.constraints = RigidbodyConstraints.None;
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
        rb.velocity = StoredVelocity;
        rb.angularVelocity = StoredAngularVelocity;
        isFreeze = false;
        StoredVelocity = Vector3.zero;
        StoredAngularVelocity = Vector3.zero;
    }


    public void environmentalDMG(Vector3 collisionPos)
    {
        if (IsGodmode == false)
        {
            TakeDamages(5);

            rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);

            rb.AddRelativeForce(-Vector3.up * 5, ForceMode.Impulse);
            /// faire un truc qui bump dans le sens oppos� au truc qui d�clenche, faire clignotter le joueur un peux et ajouter des d�gats, mettre un fx de son ?

            SetGodMode(.2f);
        }


    }

    public void SetGodMode(float Duration)
    {

        if (IsGodmode == false)
        {
            IsGodmode = true;
            Invoke("UnsetGodMode", Duration);

        }


    }

    public void UnsetGodMode()
    {
        if (IsGodmode == true)
        {
            PilotAnimator.ResetTrigger("CancelFigure");
            IsGodmode = false;


        }

    }


    public void RefreshEngineSound()
    {

        AudioSource motorsource = MotorSound.GetComponent<AudioSource>();

        if (rb.velocity.z > 0)
        {
            Pitch = rb.velocity.z / 3;

        }

        if (rb.velocity.z < 0)
        {
            Pitch = -rb.velocity.z / 3;

        }

        float Pitchclamped = Mathf.Clamp(Pitch, 1, 3);
        float PitchLerped = Mathf.Lerp(motorsource.pitch, Pitchclamped, pitchspeed * Time.deltaTime);

        motorsource.pitch = PitchLerped;
    }

    // GESTION DU BUMP
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bike")
        {

            if (isRushing == true) // si ça touche pendant le finish
            {
                DamageSFX.GetComponent<AudioSource>().Play();
                StartCoroutine(gameMaster.mainCam.GetComponent<Cam_MultiFollow>().LookAtTarget(1f, gameObject));
                collision.gameObject.GetComponent<BikeController>().pilotSkin.GetComponent<Pilot_RagdollController>().EnableRagdoll();
                EndFinisherMode(0);

            }
            else
            {
                DamageSFX.GetComponent<AudioSource>().Play();
                Vector3 bonkPos = collision.transform.position + (collision.transform.position - gameObject.transform.position) / 2;

                rb.AddForce((transform.position - collision.transform.position) * 5, ForceMode.Impulse);

                // addforce transform.position - collision.transform.position
            }

        }
    }


    void CalculateModelRotation()
    {

        if (finisherState == false)
        {
            Vector3 RightRotation = new Vector3(BikeSkin.transform.localRotation.x, 180f, BikeSkin.transform.localRotation.y);

            Vector3 LeftRotation = new Vector3(BikeSkin.transform.localRotation.x, 0f, BikeSkin.transform.localRotation.y);

            Vector3 currentAngle = BikeSkin.transform.localEulerAngles;

            if (frontWheel.GetComponent<Groundchecker>().isgrounded == true || rearWheel.GetComponent<Groundchecker>().isgrounded == true)
            {



                if (rb.velocity.z > 0)
                {
                    currentAngle = new Vector3(
                    Mathf.LerpAngle(currentAngle.x, LeftRotation.x, dir_swap_speed * Time.deltaTime),/*x*/
                    Mathf.LerpAngle(currentAngle.y, LeftRotation.y, dir_swap_speed * Time.deltaTime),/*y*/
                    Mathf.LerpAngle(currentAngle.z, 0, dir_swap_speed * Time.deltaTime));/*z*/
                    BikeSkin.transform.localEulerAngles = currentAngle;

                    //bikeskeleton.GetComponent<Animator>().ResetTrigger("StartDrift");
                    //bikeskeleton.GetComponent<Animator>().SetTrigger("EndDrift");

                }

                if (rb.velocity.z < 0)
                {
                    currentAngle = new Vector3(
                    Mathf.LerpAngle(currentAngle.x, RightRotation.x, dir_swap_speed * Time.deltaTime),
                    Mathf.LerpAngle(currentAngle.y, RightRotation.y, dir_swap_speed * Time.deltaTime),
                    Mathf.LerpAngle(currentAngle.z, 0, dir_swap_speed * Time.deltaTime));

                    BikeSkin.transform.localEulerAngles = currentAngle;

                    //   bikeskeleton.GetComponent<Animator>().ResetTrigger("StartDrift");
                    //   bikeskeleton.GetComponent<Animator>().SetTrigger("EndDrift");
                }
            }
            else if (frontWheel.GetComponent<Groundchecker>().isgrounded == false && rearWheel.GetComponent<Groundchecker>().isgrounded == false)
            {
                // si du coup les roues touchent pas, se fier à la rotation actuelle pour snapper
                if (BikeSkin.transform.localEulerAngles.y <= 90)
                {
                    currentAngle = new Vector3(
                    Mathf.LerpAngle(currentAngle.x, LeftRotation.x, dir_swap_speed * Time.deltaTime),/*x*/
                    Mathf.LerpAngle(currentAngle.y, LeftRotation.y, dir_swap_speed * Time.deltaTime),/*y*/
                    Mathf.LerpAngle(currentAngle.z, 0, dir_swap_speed * Time.deltaTime));/*z*/
                    BikeSkin.transform.localEulerAngles = currentAngle;

                }
                if (BikeSkin.transform.localEulerAngles.y > 90)
                {
                    currentAngle = new Vector3(
                    Mathf.LerpAngle(currentAngle.x, RightRotation.x, dir_swap_speed * Time.deltaTime),
                    Mathf.LerpAngle(currentAngle.y, RightRotation.y, dir_swap_speed * Time.deltaTime),
                    Mathf.LerpAngle(currentAngle.z, 0, dir_swap_speed * Time.deltaTime));

                    BikeSkin.transform.localEulerAngles = currentAngle;



                }

            }
        }




    }

    public void Jump()
    {

        if (frontWheel.GetComponent<Groundchecker>().isgrounded == true || rearWheel.GetComponent<Groundchecker>().isgrounded == true)
        {
            JumpAudio.Play();
            rb.AddForce(Vector3.up * jumpHeight, ForceMode.Impulse);
        }

    }



    public void ActivateEnergyWave()
    {
        FinisherAura.SetActive(true);
    }


    public void endfinishAnim()
    {

        pilotSkin.GetComponent<Animator>().SetBool("SwordState", false);

    }


}


