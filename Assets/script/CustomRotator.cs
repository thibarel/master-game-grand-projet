using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomRotator : MonoBehaviour
{

    public Vector3 desiredRotation;
    public float speed = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(desiredRotation * (speed * Time.deltaTime));
    }
}
