using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimReverser : MonoBehaviour
{


    public void ReverseAnim()
    {
        if (gameObject.GetComponent<Animator>().speed == 1)
        {
            gameObject.GetComponent<Animator>().speed = -1;
        }
        else
        {
            gameObject.GetComponent<Animator>().speed = 1;

        }
     
    }
}
