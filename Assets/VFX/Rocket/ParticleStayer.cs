using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class ParticleStayer : MonoBehaviour
{

   public void EndParticle(){
       gameObject.GetComponent<VisualEffect>().Stop();
       Invoke("DestroyItem",2);
   }


void DestroyItem(){
    Destroy(gameObject);
}

}
