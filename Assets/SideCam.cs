using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideCam : MonoBehaviour
{

    public GameObject owner;
    GameMaster gm;
    public Vector3 Offset;
    Camera Self;

    // Start is called before the first frame update
    void Start()
    {
        Self = GetComponent<Camera>();
        Offset = transform.localPosition;
        transform.parent = null;
        owner.GetComponent<BikeController>().gameMaster.CamList.Add(Self);
        gm = owner.GetComponent<GameMaster>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if(owner == null)
        {
           // gm.CamList.Remove(Self);
            Destroy(gameObject);
        }
        else
        {
            gameObject.transform.position = owner.transform.position + Offset;
        }
       
    }
}
