using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BGselector : MonoBehaviour
{
    public Sprite BG1;
    public Sprite BG2;
    public Sprite BG3;
    public Sprite BG4;
    public Sprite BG5;

    public Sprite BG6;

    void Update()
    {
        if(EventSystem.current.currentSelectedGameObject != null)
        {
            if (EventSystem.current.currentSelectedGameObject.name == "Map1")
            {
                gameObject.GetComponent<Image>().sprite = BG1;
            }
            if (EventSystem.current.currentSelectedGameObject.name == "Map2")
            {
                gameObject.GetComponent<Image>().sprite = BG2;
            }
            if (EventSystem.current.currentSelectedGameObject.name == "Map3")
            {
                gameObject.GetComponent<Image>().sprite = BG3;
            }
            if (EventSystem.current.currentSelectedGameObject.name == "Map4")
            {
                gameObject.GetComponent<Image>().sprite = BG4;
            }

            if (EventSystem.current.currentSelectedGameObject.name == "Map5")
            {
                gameObject.GetComponent<Image>().sprite = BG5;
            }

            if (EventSystem.current.currentSelectedGameObject.name == "Map6")
            {
                gameObject.GetComponent<Image>().sprite = BG6;
            }
        }

    }
}
