using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimCollector : MonoBehaviour
{
    // Start is called before the first frame update
    public void ActivateEnergyWave(){
        gameObject.GetComponentInParent<BikeController>().ActivateEnergyWave();
    }
}
